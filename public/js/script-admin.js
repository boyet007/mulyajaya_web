Dropzone.options.addImages = {
    paramName: 'file',
    maxFilesize: 5, // MB
    acceptedFiles: 'image/*',

};

// var handleDropzoneFileUpload = {
//      handleError: function(response){
//         console.log(response);
//      },
//     handleSuccess: function(response){
//         console.log(response);
//     }
// };


$(function(){
	var interval = setInterval(function() {
		var momentNow = moment();
		$('#tanggal').html(momentNow.format('DD-MM-YYYY'));
		$('#waktu').html(momentNow.format('HH:mm:ss'));
	}, 100);


    // Dropzone.options.myDropzone = {
    //     paramName: 'file',
    //     maxFilesize: 5, // MB
    //     maxFiles: 200,
    //     acceptedFiles: ".jpeg,.jpg,.png,.gif",
    //     init: function() {
    //         this.on("success", function(file, response) {
    //             var a = document.createElement('span');
    //             a.className = "thumb-url btn btn-primary";
    //             a.setAttribute('data-clipboard-text',"{{url('/images')}}'+'/"+response);
    //             a.innerHTML = "copy url";
    //             file.previewTemplate.appendChild(a);
    //         });
    //     }
    // };


	$('#gambar-artikel-src').change(function(){
		var x = $(this).val();
		$('#gambar-artikel').fadeOut(300, function(){
			$(this).attr('src', '/images/' + x);
		}).fadeIn(300);
	});

	$('#gambar-produk-src').change(function(){
		var x = $(this).val();
		$('#gambar-produk').fadeOut(300, function(){
			$(this).attr('src', '/images/produk/' + x);
		}).fadeIn(300);
	});

	$('#gambar-galeri-src').change(function(){
		var x = $(this).val();
		$('#gambar-galeri').fadeOut(300, function(){
			$(this).attr('src', '/images/galeri/' + x);
		}).fadeIn(300);
	});

	$('#gambar-carousel-src').change(function(){
		var x = $(this).val();
		$('#gambar-carousel').fadeOut(100, function(){
			$(this).attr('src', '/images/carousel/' + x);
		}).fadeIn(300);
	});

    $('#gambar-mesin-src').change(function(){
        var x = $(this).val();
        $('#gambar-mesin').fadeOut(100, function(){
            $(this).attr('src', '/images/mesin/' + x);
        }).fadeIn(300);
    });

	$("#reset").click(function(){
		document.location.reload(true);
	});

	$('#form-artikel').validate({
		rules:{
			judul : 'required',
			isi : 'required'
		},
		messages :{
			judul : 'Silahkan masukkan judul',
			isi : 'Silahkan masukkan isi'
		}
	});

	$('#form-kategori').validate({
		rules:{
			nama : 'required',
			keterangan : 'required'
		},
		messages :{
			nama : 'Silahkan masukkan nama',
			keterangan : 'Silahkan masukkan keterangan'
		}
	});

	$('#form-produk').validate({
		rules:{
			kat_id : 'required',
			nama : 'required',
			gambar: 'required',
			keterangan : 'required',
		},
		messages :{
			kat_id : 'Silahkan masukkan kategori',
			gambar : 'Silahkan pilih gambar',
			nama : 'Silahkan masukkan nama',
			keterangan : 'Silahkan masukkan keterangan'
		}
	});

	$('#form-galeri').validate({
		rules:{
			nama : 'required',
			path : 'required',
			keterangan : 'required',
		},
		messages :{
			nama : 'Silahkan masukkan nama',
			path : 'Silahkan pilih path gambar',
			keterangan : 'Silahkan masukkan keterangan'
		}
	});

	$('#form-carousel').validate({
		rules:{
			nama : 'required',
			gambar : 'required'
		},
		messages :{
			nama : 'Silahkan masukkan nama',
			gambar : 'Silahkan pilih gambar'
		}
	});

    $('#form-mesin').validate({
        rules:{
            nama : 'required',
            gambar : 'required'
        },
        messages :{
            nama : 'Silahkan masukkan nama',
            gambar : 'Silahkan pilih gambar'
        }
    });

	$('.form-setting').validate({
		rules:{
			nama_perusahaan : 'required',
      tag : 'required',
			nama_tempat : 'required',
      blok : 'required',
      nama_jalan : 'required',
			kota : 'required',
			kode_pos : 'required',
			telp : 'required',
      fax : 'required',
			email : 'required',
			keterangan : 'required'
		},
		messages:{
			nama_perusahaan : 'Silahkan masukkan nama perusahaan',
      tag : 'Silahkan masukkan tag',
			nama_tempat : 'Silahkan masukkan nama lokasi',
      blok : 'Silahkan masukkan blok',
      nama_jalan : 'Silahkan masukkan nama jalan',
			kota : 'Silahkan masukkan nama kota',
			kode_pos : 'Silahkan masukkan kode pos',
			telp : 'Silahkan masukkan no telp',
      fax : 'Silahkan masukkan no fax',
			email : 'Silahkan masukkan nama email',
			keterangan : 'Silahkan masukkan nama keterangan'
		}
	});

    // $('.dropzone-artikel').dropzone({
		// url:'/public/images/'
    // });
    //
    // $('.dropzone-produk').dropzone({
    //     url:'/public/images/produk/'
    // });
    //
    // $('.dropzone-galeri').dropzone({
    //     url:'/public/images/galeri/'
    // });
    //
    // $('.dropzone-carousel').dropzone({
    //     url:'/public/images/carousel/'
    // });

    $(".group2").colorbox({
        rel:'group2',
        transition:'fade'
    });
});
