
$(function(){

	
	// $('.zooming').hover(function(){
	// 	$(this).css('cursor', 'pointer');
	// 	$(this).animate({
	// 		width: '100%',
	// 		height: '100%'
	// 	}, 'slow');
	// }, function(){
	// 	$(this).animate({
	// 		width:'28%'
	// 	}, 'slow');
	// });

	$('.carousel').carousel();

	$(".dropdown").hover(function () {
		$(".dropdown-menu").stop().slideToggle('fast');
	});

	$(".group1").colorbox({
		width: '150%',
		height: '100%',
		rel:'group2', 
		transition:'fade'
	});

	$(".group2").colorbox({
		rel:'group2', 
		transition:'fade'
	});

	$(".group3").colorbox({
		rel:'group3', 
		transition:"elastic",
		current: false
	});

	$('.pop').on('click', function() {
			// var x = $(this).find('img').attr('src');
			// console.log(x);

			//console($(this).find('img').attr('src'));
			$('.imagepreview').attr('src', $(this).find('img').attr('src'));
			$('#myModal').modal('show');   
		});			

	$('#form-kontak').validate({
		reles:{
			name : "required",
			company : "required", 
			email : {
				required : true,
				email : true
			},
			pesan : "required",
			message : {
				required : true
			}
		},
		messages:{
			name : "Silahkan masukkan nama anda",
			company : "Silahkan masukkan perusahaan anda",
			email : {
				required : "Silahkan masukkan email anda",
				email : "Silahkan masukkan alamat email yang benar"
			},
			message : {
				required : "Silahkan masukkan pesan anda"
			}
		}
	});

	$('#form-login').validate({
		rules:{
			username : 'required',
			password : 'required'
		}, 
		messages :{
			username : 'Silahkan masukkan username anda',
			password : 'Silahkan masukkan password anda'
		}
	});

	jQuery.extend(jQuery.validator.messages, {
		required: "Silahkan masukkan pesan anda"
	});

});
