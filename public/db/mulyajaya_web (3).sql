-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2018 at 07:47 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mulyajaya_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `carousel`
--

CREATE TABLE `carousel` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carousel`
--

INSERT INTO `carousel` (`id`, `nama`, `gambar`) VALUES
(1, 'Bushing besi', 'carousel01.jpg'),
(2, 'Motto', 'carousel02.jpg'),
(3, 'Hasil Kerja', 'carousel03.jpg'),
(4, 'Mur', 'carousel04.jpg'),
(5, 'packing', 'carousel1.jpg'),
(6, 'Bahan sebelum di proses', 'hasil.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `foto`
--

CREATE TABLE `foto` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `galeri`
--

CREATE TABLE `galeri` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galeri`
--

INSERT INTO `galeri` (`id`, `nama`, `path`, `keterangan`) VALUES
(17, 'Universal Milling Machine', 'galeri01.jpg', 'tidak ada..'),
(18, 'Mesin Grinding', 'galeri02.jpg', 'Mesin Potong'),
(19, 'Suasana Pabrik', 'galeri03.jpg', 'tidak ada'),
(20, 'Keadaaan depan pabrik', 'galeri04.jpg', 'tidak ada'),
(21, 'Hasil Produk', 'galeri05.jpg', 'tidak ada'),
(22, 'Kompresor', 'galeri_01.jpg', 'tidak ada'),
(23, 'Gear', 'galeri_02.jpg', 'tidak ada'),
(24, 'Bahan', 'P_20180611_102859.jpg', '-'),
(25, 'milling', 'P_20180611_112549.jpg', '-'),
(26, 'Office', 'P_20180611_150152.jpg', '-'),
(27, 'Mesin CNC', 'P_20180605_100900.jpg', '-'),
(28, 'Packing', '222.jpg', '-');

-- --------------------------------------------------------

--
-- Table structure for table `jasa`
--

CREATE TABLE `jasa` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jasa`
--

INSERT INTO `jasa` (`id`, `nama`) VALUES
(1, 'Fraist'),
(2, 'Milling'),
(3, 'Stick'),
(4, 'Skraft'),
(5, 'Bubut'),
(6, 'Roda Gigi Lurus'),
(7, 'Wormgear'),
(8, 'Internal Gear'),
(9, 'Gearbox'),
(10, 'Spiral Bevel Gear');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama`, `keterangan`) VALUES
(1, 'Milling', 'tidak ada'),
(2, 'Stick', 'Tidak ada'),
(3, 'Skraft', 'Iste non qui vel quos eveniet dolores iusto sunt eos consequuntur quia voluptatem sit perspiciatis consequatur soluta architecto pariatur sit est voluptate repellendus doloribus architecto dolorum error ullam quo voluptas debitis modi distinctio aut.'),
(4, 'Fraist', 'Ullam dolorem officiis nobis aut explicabo quod omnis dolorem assumenda quia inventore et porro quas culpa sit nobis dolores saepe aut sint repudiandae ab consequatur necessitatibus consectetur consequuntur voluptas sed blanditiis ad illo ipsum et velit sed dolores quo sit odit voluptate sunt fugiat eum.'),
(5, 'Bubut', 'Tidak ada'),
(6, 'CNC', 'TIdak ada');

-- --------------------------------------------------------

--
-- Table structure for table `kontak`
--

CREATE TABLE `kontak` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `perusahaan` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pesan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kontak`
--

INSERT INTO `kontak` (`id`, `nama`, `perusahaan`, `email`, `pesan`, `waktu`) VALUES
(1, 'Assumenda.', 'Tempore quo et.', 'halie16@yahoo.com', 'Ut voluptas aliquid quod eum perspiciatis omnis molestiae voluptatem doloremque esse consectetur sunt iusto aspernatur sed harum eos dolorem deleniti rem et nostrum quis ut nemo similique ea eum enim unde autem eveniet fugiat laudantium numquam voluptatem quas.', '2017-03-15 02:52:59'),
(2, 'Illum.', 'Autem modi aut.', 'schimmel.coralie@lowe.com', 'Quidem repudiandae quos enim sint autem adipisci ut est qui porro cumque sed nihil repellat in sequi ex non aut a placeat sed in quidem earum magni quos sequi est labore quia aut itaque vel ut id reprehenderit dicta.', '2015-11-23 19:15:17'),
(3, 'Vel quas.', 'Nisi autem.', 'frankie85@yahoo.com', 'Nesciunt sunt labore placeat molestiae id similique quo doloremque facilis recusandae nihil molestiae debitis minus fugiat voluptatum quisquam sit esse.', '2014-03-12 01:57:28'),
(4, 'Ducimus.', 'Eum.', 'kiarra24@hotmail.com', 'Omnis non in aut commodi perferendis consectetur et non est suscipit beatae et accusamus itaque quo necessitatibus qui nihil culpa facilis et dolorem optio et adipisci qui aut sapiente impedit ipsa dolorem laborum qui ipsa quia mollitia.', '2014-03-06 22:56:15'),
(5, 'Quae eius.', 'Dolores est.', 'kjohnston@monahan.com', 'Ad dolores et consequatur cumque eos ut ut sunt qui autem reiciendis voluptas sunt commodi et omnis dolores reprehenderit recusandae sit inventore earum deserunt consequatur enim ipsum optio rerum quis recusandae cupiditate voluptatem nisi rerum vel qui impedit sed corrupti.', '2012-04-05 22:26:28'),
(6, 'Ex ut aut.', 'Vero.', 'winfield68@armstrong.com', 'Sint quisquam nulla explicabo delectus id et et omnis et aut voluptatibus architecto ut aperiam quisquam amet omnis sapiente voluptatibus rerum voluptatem est in ut quisquam corrupti veniam odio quis temporibus debitis.', '2015-11-29 11:33:28'),
(7, 'Ratione.', 'Omnis adipisci.', 'adrain07@schimmel.com', 'Saepe voluptatem et amet voluptas sit eligendi sit qui natus neque et maxime voluptatum placeat cumque qui sit earum enim cumque expedita eius et.', '2009-04-26 12:07:49'),
(8, 'Rerum eum.', 'Suscipit quod.', 'domenico.nolan@hotmail.com', 'Excepturi a quis dicta sed eum et facilis nostrum vero ex fugiat asperiores omnis velit sint dicta delectus non ex quos voluptate sapiente minima rerum dolor.', '2017-06-09 19:40:35'),
(9, 'Excepturi.', 'Dolorem.', 'woconnell@yahoo.com', 'Soluta maiores itaque provident eos non quis qui accusamus excepturi nesciunt et aut numquam ut deleniti unde aliquam qui.', '2014-06-25 13:54:25'),
(10, 'Quis.', 'Nihil aut.', 'hdooley@kuhn.com', 'Ea cumque accusamus praesentium sed et et eum et saepe accusamus quaerat sit in non necessitatibus consequatur voluptatem optio inventore sed blanditiis enim a culpa aliquam optio eum ea est sunt molestias iure corrupti dicta dolorum esse iure ipsum omnis consequuntur.', '2017-12-09 17:26:45'),
(11, 'Illo.', 'Est a beatae.', 'hodkiewicz.dario@muller.com', 'Ut voluptatem autem doloremque vitae sint et deleniti corrupti quia quia molestiae dolorem debitis molestiae quibusdam eum sunt exercitationem eum.', '2017-01-11 14:59:30'),
(12, 'Accusamus.', 'Facere dicta.', 'cmarks@mueller.com', 'Iusto est dolorem esse voluptas quo et ut quam facere commodi voluptatem consectetur nesciunt fugiat reiciendis eum veritatis ut eum corrupti aut laboriosam at vel ullam voluptas assumenda sapiente.', '2014-07-27 06:12:09'),
(13, 'Doloribus.', 'Aut non enim.', 'jeffry.dibbert@hotmail.com', 'Et impedit dolore sint deserunt incidunt nulla in veniam et ea necessitatibus explicabo animi est sit sapiente adipisci fuga.', '2010-08-19 15:38:24'),
(14, 'Officia.', 'Accusamus vel.', 'flavio.kihn@feeney.net', 'Corrupti qui qui eum hic facere totam hic qui sint veritatis autem possimus ipsum pariatur sed ut quibusdam in est rem impedit magnam aut incidunt sit eveniet adipisci qui quisquam itaque qui voluptas quod et nihil ab aut eius similique beatae.', '2013-07-06 03:45:23'),
(15, 'Officiis.', 'Doloribus quas.', 'ospinka@hotmail.com', 'Est exercitationem quisquam quae minus atque quibusdam voluptatum ut cupiditate laudantium eaque excepturi voluptatem mollitia rerum doloremque placeat non dolor quo accusamus rerum consectetur soluta maiores incidunt quia ipsa vitae illo mollitia voluptatibus et temporibus nihil eaque aut ea dolor ullam corporis.', '2009-04-28 13:08:31');

-- --------------------------------------------------------

--
-- Table structure for table `konten`
--

CREATE TABLE `konten` (
  `id` int(10) UNSIGNED NOT NULL,
  `bagian` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `konten`
--

INSERT INTO `konten` (`id`, `bagian`, `judul`, `gambar`, `isi`, `created_at`, `updated_at`) VALUES
(1, 'muka_1', 'Profile Perusahaan', 'artikel_01.jpg', 'CV Mulia Jaya Mandiri didirikan pada tahun 2000. Pada awalnya kami hanya mempunyai 3 karyawan saja. Lambat laun kami berkembang pesat dan kini telah mempunyai jumlah karyawan 10 orang. Fasitilitas kami terdiri dari berbagai line up mesin untuk memproduksi part dan objek sesuai dengan gambar dari klien. Saat ini kami memfokuskan bisnis untuk membuat part part dan suplier material dengan berbagai mesin proses yang menunjang bisnis kami. Kami juga menjual berbagai part berbagai macam mesin. Dengan pengalaman lebih dari 15 tahun, kami  dapat diandalkan untuk memberikan produk dan service yang diinginkan oleh klien atau customer kami.', '2018-06-08 07:08:40', '2018-06-12 03:40:03'),
(2, 'muka_2', 'Perbaikan mesin industri', 'repair.jpg', 'Mesin yang digunakan terus menerus dan dapat mengakibatkan kerusakan terkadang beberapa sparepart yang digunakan keberadaanya langka dipasaran sehingga anda harus membeli dari pabrik pembuat mesinnya. Padahal mayoritas pembuat mesin adalah dari luar negeri sehingga diperlukan waktu yanglama untuk mengimpornya akhirnya akan menghambat proses produksi. Kami hadir untuk menyelesaikan masalah tersebut. Tim kami sudah berpengalaman membuat part berdasarkan bentuk yang di berikan klien atau gambar baik dalam bentuk 2D atau 3D. Juga kami berpengalaman membongkar berbagai macam mesin sehingga klien tidak perlu repot repot membongkar nya sendiri.', '2018-06-08 07:08:40', '2018-06-12 04:02:41'),
(3, 'muka_3', 'Mesin yang lengkap dan memadai', 'lineup_mesin.jpg', 'Dengan berbagai macam mesin workshop untuk kebutuhan milling, bubut, fraist, scraft, CNC dll. Kami siap mengerjakan berbagai macam part dari client dari yang kecil hingga besar, sampai halus.Semua bisa kami kerjakan !!!. Baja, Baja antikarat, Cooper, Alumunium, Brass, Teflon dll. Silahkan hubungi kami sekarang juga untuk mendapatkan penawaran terbaik dari kami.', '2018-06-08 07:08:40', '2018-06-12 04:47:47');

-- --------------------------------------------------------

--
-- Table structure for table `mesin`
--

CREATE TABLE `mesin` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mesin`
--

INSERT INTO `mesin` (`id`, `nama`, `gambar`) VALUES
(1, 'CNC YCM Type 6T-250 B', '1 lathe ycm supermax 6t.jpg'),
(2, 'Grinda OTT', '1 ott.jpg'),
(3, 'Bend Saw 250-HA', 'bend saw.jpg'),
(4, 'Gear Hobber Y3150 E', '2 gear hobbing.jpg'),
(5, 'CNC Lathe Takisawa TC 100 T8L3', '5 cnc lathe.jpg'),
(6, 'Welding Machine SA600', '5 spolt welding.jpg'),
(7, 'Universal Milling X6125', '9 manual milling.jpg'),
(8, 'Takisawa TAL-460 Lathe', 'bubut manual no 8.jpg'),
(9, 'Manual Bubut', 'bubut manual.jpg'),
(11, 'Press Machine up to 120 ton', 'press.jpg'),
(12, 'Vertical Machine Milling 1050 B', 'vertical milling cnc.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(12, '2018_05_24_100646_create_page_views_table', 1),
(24, '2017_12_25_050420_create_visitortracker_visits_table', 3),
(58, '2014_10_12_000000_create_users_table', 4),
(59, '2014_10_12_100000_create_password_resets_table', 4),
(60, '2018_05_08_062848_create_produks_table', 4),
(61, '2018_05_08_062912_create_kategoris_table', 4),
(62, '2018_05_08_062948_create_kontens_table', 4),
(63, '2018_05_08_070250_create_fotos_table', 4),
(64, '2018_05_08_070625_create_setting_table', 4),
(65, '2018_05_10_050549_create_carousels_table', 4),
(66, '2018_05_12_084802_create_jasas_table', 4),
(67, '2018_05_14_061148_create_galeris_table', 4),
(68, '2018_05_16_061505_create_kontaks_table', 4),
(69, '2018_06_09_164858_create_mesin_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `page-views`
--

CREATE TABLE `page-views` (
  `id` int(10) UNSIGNED NOT NULL,
  `visitable_id` bigint(20) UNSIGNED DEFAULT NULL,
  `visitable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page-views`
--

INSERT INTO `page-views` (`id`, `visitable_id`, `visitable_type`, `ip_address`, `created_at`, `updated_at`) VALUES
(18, NULL, 'App\\Konten', '192.168.1.77', '2018-05-24 03:25:33', '2018-05-24 03:25:33'),
(19, NULL, 'App\\Konten', '192.168.1.77', '2018-05-24 03:25:45', '2018-05-24 03:25:45'),
(23, NULL, 'App\\Konten', '192.168.1.77', '2018-05-24 03:45:07', '2018-05-24 03:45:07'),
(24, NULL, 'App\\Konten', '192.168.1.61', '2018-05-24 03:46:51', '2018-05-24 03:46:51'),
(25, NULL, 'App\\Konten', '192.168.1.61', '2018-05-24 03:47:41', '2018-05-24 03:47:41'),
(26, NULL, 'App\\Konten', '192.168.1.77', '2018-05-24 03:48:16', '2018-05-24 03:48:16'),
(27, NULL, 'App\\Konten', '192.168.1.77', '2018-05-24 03:49:04', '2018-05-24 03:49:04'),
(28, NULL, 'App\\Galeri', '192.168.1.77', '2018-05-24 03:59:10', '2018-05-24 03:59:10'),
(29, NULL, 'App\\Kontak', '192.168.1.77', '2018-05-24 03:59:16', '2018-05-24 03:59:16');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(10) UNSIGNED NOT NULL,
  `kat_id` int(11) NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_perusahaan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_tempat` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blok` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_jalan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_pos` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `nama_perusahaan`, `tag`, `nama_tempat`, `blok`, `nama_jalan`, `kota`, `kode_pos`, `telp`, `fax`, `email`, `keterangan`, `gambar`) VALUES
(1, 'CV Mulia Jaya Mandiri', 'Bubut, Milling dan Sparepart Produksi', 'Green Agung Sedayu', 'BizPark Blok GS7 No. 18', 'Jl. Cakung Cilincing Timur Raya Km 2', 'Jakarta', '13910', '021 2246 2689', '021  2246 3665', 'mulyajaya@yahoo.com', 'tidak ada', 'logo.jpg'),
(2, 'PD Lancar Semesta Teknik', '-', 'Lindeteves Trade Centre', 'Lantai SB Blok A1 No. 18', 'Jl. Hayam Wuruk 127', 'Jakarta', '11180', '021 6231 7405', '-', 'mulyajaya@yahoo.com', 'tidak ada', 'logo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin_web@mulyajaya.com', '$2y$10$Xl1ro77mD/246grIb2idqOEbqwD8JSvAnGcy8DV8GKi5WwpVWvQv2', 'C0z22lNLB1h25k9U1sLMfdNuUctAObad7ausSCyXqojaGIknuzclrdvsFENk', '2018-06-08 07:08:47', '2018-06-08 07:08:47');

-- --------------------------------------------------------

--
-- Table structure for table `visitortracker_visits`
--

CREATE TABLE `visitortracker_visits` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_ajax` tinyint(1) NOT NULL DEFAULT '0',
  `url` text COLLATE utf8mb4_unicode_ci,
  `referer` text COLLATE utf8mb4_unicode_ci,
  `user_agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_desktop` tinyint(1) NOT NULL DEFAULT '0',
  `is_mobile` tinyint(1) NOT NULL DEFAULT '0',
  `is_bot` tinyint(1) NOT NULL DEFAULT '0',
  `bot` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os_family` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `os` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `browser_family` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `browser` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `is_login_attempt` tinyint(1) NOT NULL DEFAULT '0',
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `lat` double DEFAULT NULL,
  `long` double DEFAULT NULL,
  `browser_language_family` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `browser_language` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visitortracker_visits`
--

INSERT INTO `visitortracker_visits` (`id`, `user_id`, `ip`, `method`, `is_ajax`, `url`, `referer`, `user_agent`, `is_desktop`, `is_mobile`, `is_bot`, `bot`, `os_family`, `os`, `browser_family`, `browser`, `is_login_attempt`, `country`, `country_code`, `city`, `lat`, `long`, `browser_language_family`, `browser_language`, `created_at`, `updated_at`) VALUES
(1, 1, '192.168.1.77', 'GET', 0, 'http://192.168.1.61', 'http://192.168.1.61/login', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', 1, 0, 0, NULL, 'windows', 'Windows 7', 'chrome', 'Chrome 66.0', 0, '', '', '', NULL, NULL, 'id', 'id-ID', '2018-05-24 05:05:58', '2018-05-24 05:05:58'),
(2, 1, '192.168.1.77', 'GET', 0, 'http://192.168.1.61', 'http://192.168.1.61/', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', 1, 0, 0, NULL, 'windows', 'Windows 7', 'chrome', 'Chrome 66.0', 0, '', '', '', NULL, NULL, 'id', 'id-ID', '2018-05-24 05:08:26', '2018-05-24 05:08:26'),
(3, 1, '192.168.1.77', 'GET', 0, 'http://192.168.1.61', 'http://192.168.1.61/', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', 1, 0, 0, NULL, 'windows', 'Windows 7', 'chrome', 'Chrome 66.0', 0, '', '', '', NULL, NULL, 'id', 'id-ID', '2018-05-24 05:13:47', '2018-05-24 05:13:47'),
(4, 1, '192.168.1.77', 'GET', 0, 'http://192.168.1.61/kategori/2', 'http://192.168.1.61/', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', 1, 0, 0, NULL, 'windows', 'Windows 7', 'chrome', 'Chrome 66.0', 0, '', '', '', NULL, NULL, 'id', 'id-ID', '2018-05-24 05:14:12', '2018-05-24 05:14:12'),
(5, 1, '192.168.1.77', 'GET', 0, 'http://192.168.1.61/kategori/2', 'http://192.168.1.61/', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', 1, 0, 0, NULL, 'windows', 'Windows 7', 'chrome', 'Chrome 66.0', 0, '', '', '', NULL, NULL, 'id', 'id-ID', '2018-05-24 05:14:23', '2018-05-24 05:14:23'),
(6, 1, '192.168.1.77', 'GET', 0, 'http://192.168.1.61/kategori/1', 'http://192.168.1.61/kategori/2', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', 1, 0, 0, NULL, 'windows', 'Windows 7', 'chrome', 'Chrome 66.0', 0, '', '', '', NULL, NULL, 'id', 'id-ID', '2018-05-24 05:14:35', '2018-05-24 05:14:35'),
(7, 1, '192.168.1.77', 'GET', 0, 'http://192.168.1.61/galeri', 'http://192.168.1.61/kategori/1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', 1, 0, 0, NULL, 'windows', 'Windows 7', 'chrome', 'Chrome 66.0', 0, '', '', '', NULL, NULL, 'id', 'id-ID', '2018-05-24 05:14:56', '2018-05-24 05:14:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carousel`
--
ALTER TABLE `carousel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jasa`
--
ALTER TABLE `jasa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kontak`
--
ALTER TABLE `kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `konten`
--
ALTER TABLE `konten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mesin`
--
ALTER TABLE `mesin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page-views`
--
ALTER TABLE `page-views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `visitortracker_visits`
--
ALTER TABLE `visitortracker_visits`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carousel`
--
ALTER TABLE `carousel`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `foto`
--
ALTER TABLE `foto`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `galeri`
--
ALTER TABLE `galeri`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `jasa`
--
ALTER TABLE `jasa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kontak`
--
ALTER TABLE `kontak`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `konten`
--
ALTER TABLE `konten`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mesin`
--
ALTER TABLE `mesin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `page-views`
--
ALTER TABLE `page-views`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `visitortracker_visits`
--
ALTER TABLE `visitortracker_visits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
