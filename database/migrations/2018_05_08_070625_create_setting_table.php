<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_perusahaan', 50);
            $table->string('tag', 100);
            $table->string('nama_tempat', 50);
            $table->string('blok', 50);
            $table->string('nama_jalan', 100);
            $table->string('kota', 25);
            $table->string('kode_pos', 10);
            $table->string('telp', 25);
            $table->string('fax', 25);
            $table->string('email', 50);
            $table->text('keterangan');
            $table->string('gambar', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');
    }
}
