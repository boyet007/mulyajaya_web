<?php

use Illuminate\Database\Seeder;

class JasaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('jasa')->insert([
    		'nama' => 'Fraist'
    	]);

    	DB::table('jasa')->insert([
    		'nama' => 'Milling'
    	]);

    	DB::table('jasa')->insert([
    		'nama' => 'Stick'
    	]);

    	DB::table('jasa')->insert([
    		'nama' => 'Skraft'
    	]);

    	DB::table('jasa')->insert([
    		'nama' => 'Bubut'
    	]);
    }
}
