<?php

use Illuminate\Database\Seeder;

class KontenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();

    	$time = Carbon\Carbon::now();

      $path = array('img-01.jpg', 'img-02.jpg', 'img-03.jpg', 'img-04.jpg', 'img-05.jpg', 'img-06.jpg', 'img-07.jpg', 'img-08.jpg', 'img-09.jpg', 'img-10.jpg');

      DB::table('konten') -> insert([
          'bagian' => 'muka_1',
          'judul' => 'judul_muka_1',
          'gambar' => $faker->randomElement($path),
          'isi' => $faker->sentence(200),
          'created_at' => $time->toDateTimeString(),
          'updated_at' => $time->toDateTimeString()
      ]);

      DB::table('konten') -> insert([
          'bagian' => 'muka_2',
          'judul' => 'judul_muka_2',
          'gambar' => $faker->randomElement($path),
          'isi' => $faker->sentence(200),
          'created_at' => $time->toDateTimeString(),
          'updated_at' => $time->toDateTimeString()
      ]);

      DB::table('konten') -> insert([
          'bagian' => 'muka_3',
          'judul' => 'judul_muka_3',
          'gambar' => $faker->randomElement($path),
          'isi' => $faker->sentence(200),
          'created_at' => $time->toDateTimeString(),
          'updated_at' => $time->toDateTimeString()
      ]);
  }
}
