<?php

use Illuminate\Database\Seeder;
use App\Kategori;

class ProdukTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $kat_id = Kategori::all()->pluck('id')->toArray();
        $path = array('img-01.jpg', 'img-02.jpg', 'img-03.jpg', 'img-04.jpg', 'img-05.jpg', 'img-06.jpg', 'img-07.jpg', 'img-08.jpg', 'img-09.jpg', 'img-10.jpg');


        foreach(range(1, 15) as $i) {
            DB::table('produk') -> insert([
                'kat_id' => $faker->randomElement($kat_id),
                'nama' => $faker->text($maxNbChars = 10),
                'gambar' => $faker->randomElement($path),
                'keterangan' => $faker->sentence(50)
            ]);
        }
    }
}
