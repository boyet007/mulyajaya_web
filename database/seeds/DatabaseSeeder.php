<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       $this->call(KategoriTableSeeder::class);
       $this->call(ProdukTableSeeder::class);
       $this->call(KontenTableSeeder::class);
       $this->call(SettingTableSeeder::class);
       $this->call(CaraouselTableSeeder::class);
       $this->call(JasaTableSeeder::class);
       $this->call(GaleriTableSeeder::class);
       $this->call(KontakTableSeeder::class);
       $this->call(UserTableSeeder::class);

   }

}
