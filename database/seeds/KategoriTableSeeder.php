<?php

use Illuminate\Database\Seeder;

class KategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

    	DB::table('kategori')->insert([
    		'nama' => 'Besi',
    		'keterangan' => $faker->sentence(50)
    	]);

    	DB::table('kategori')->insert([
    		'nama' => 'Emas',
    		'keterangan' => $faker->sentence(50)
    	]);

    	DB::table('kategori')->insert([
    		'nama' => 'Alumunium',
    		'keterangan' => $faker->sentence(50)
    	]);

    	DB::table('kategori')->insert([
    		'nama' => 'Timah',
    		'keterangan' => $faker->sentence(50)
    	]);
    }
}
