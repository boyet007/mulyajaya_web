<?php

use Illuminate\Database\Seeder;

class GaleriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        //create file list caraousel
        $files = File::allFiles(public_path('images/galeri'));
        $galeri = array();

        foreach ($files as $file)
		{
			array_push($galeri, basename((string)$file));
		}

		$total = count($galeri);

		for($i=0;$i<$total;$i++) {
            DB::table('galeri') -> insert([
                'nama' => $faker->text($maxNbChars = 10),
                'path' => $galeri[$i],
                'keterangan' => $faker->sentence(50)
            ]);
        }
    }
}
