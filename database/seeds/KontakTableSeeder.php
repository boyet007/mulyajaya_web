<?php

use Illuminate\Database\Seeder;

class KontakTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        foreach(range(1, 15) as $i) {
            DB::table('kontak') -> insert([
                'nama' => $faker->text($maxNbChars = 10),
                'perusahaan' => $faker->text($maxNbChars = 15), 
                'email' => $faker->email(),
                'pesan' => $faker->sentence(30),
                'waktu' => $faker->dateTimeThisDecade($max = 'now', $timezone = null)
            ]);
        }
    }
}
