<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// $faker = Faker\Factory::create();
      //
      //   DB::table('setting') -> insert([
    	// 	'nama_perusahaan' => 'CV. Mulya Jaya',
    	// 	'nama_jalan' => 'Biz Park Cakung GS 7 No 18',
    	// 	'kelurahan' => 'Bebek TNI',
    	// 	'kecamatan' => 'Cakung',
    	// 	'kota' => 'Jakarta',
    	// 	'provinsi' => 'DKI Jakarta',
    	// 	'kode_pos' => '17293',
    	// 	'telp' => '021 380 2191',
    	// 	'email' => 'mulyajaya@yahoo.com',
    	// 	'keterangan' => $faker->sentence(50),
      //       'gambar' => 'logo.jpg'
    	// ]);

      DB::table('setting') -> insert([
        'nama_perusahaan' => 'CV Mulia Jaya Mandiri',
        'tag' => 'Seni Dari Teknologi',
        'nama_tempat' => 'Green Agung Sedayu',
      	'blok' => 'BizPark Blok GS7 No. 18',
      	'nama_jalan' => 'Jl. Cakung Cilincing Timur Raya Km 2',
      	'kota' => 'Jakarta',
      	'kode_pos' => '13910',
      	'telp' => '021 2246 2689',
        'fax' => '021  2246 3665',
      	'email' => 'mulyajaya@yahoo.com',
      	'keterangan' => 'tidak ada',
        'gambar' => 'logo.jpg'
      ]);

      DB::table('setting') -> insert([
        'nama_perusahaan' => 'CV Mulia Jaya Mandiri (Toko)',
        'tag' => '-',
        'nama_tempat' => 'Lindeteves Trade Centre',
      	'blok' => 'Lantai SB Blok A1 No. 18',
      	'nama_jalan' => 'Jl. Hayam Wuruk 127',
      	'kota' => 'Jakarta',
      	'kode_pos' => '11180',
      	'telp' => '021 6231 7405',
        'fax' => '-',
      	'email' => 'mulyajaya@yahoo.com',
      	'keterangan' => 'tidak ada',
        'gambar' => 'logo.jpg'
      ]);
    }
}
