<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$time = Carbon\Carbon::now();

         DB::table('users') -> insert([
    		'username' => 'admin',
    		'email' => 'admin_web@mulyajaya.com',
    		'password' => bcrypt('203203'),
    		'remember_token' => str_random(10),
    		'created_at' => $time->toDateTimeString(),
    		'updated_at' => $time->toDateTimeString()
    	]);
    }
}
