<?php

use Illuminate\Database\Seeder;

class CaraouselTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        //create file list caraousel
        $files = File::allFiles(public_path('images/carousel'));
        $caraousel = array();
        
        foreach ($files as $file)
		{
			array_push($caraousel, basename((string)$file));
		}

		$total = count($caraousel);

		for($i=0;$i<$total;$i++) {
            DB::table('carousel') -> insert([
                'nama' => $faker->text($maxNbChars = 10),
                'gambar' => $caraousel[$i]
            ]);
        }
    }
}
