<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Konten;
use Auth;
use Storage;


class ArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    var $konten;
    var $title;

    public function __construct(){
        $this->konten = new Konten();
        $this->title = 'Data Artikel';
    }

    public function index()
    {
      $artikel_1 = $this->konten::find(1);
        $artikel_2 = $this->konten::find(2);
        $artikel_3 = $this->konten::find(3);

      $subtitle = 'Index';
      $title = $this->title;
      $user =  Auth::user()->username;

      return view('admins.artikel.index')
      ->with(compact('artikel_1'))
          ->with(compact('artikel_2'))
          ->with(compact('artikel_3'))

      ->with(compact('title'))
      ->with(compact('subtitle'))
      ->with(compact('user'));
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user =  Auth::user()->username;

        $artikel = $this->konten::find($id);
        $title = $this->title;
        $subtitle = 'Edit';
        $user =  Auth::user()->username;

        $arr_filename = $this->ambil_data_gambar();


        return view('admins.artikel.edit')
        ->with(compact('title'))
        ->with(compact('subtitle'))
        ->with(compact('artikel'))
        ->with(compact('arr_filename'))
        ->with(compact('user'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // Konten::whereId($id)->update($request->all());

        $bagian = $request->get('bagian');

        $konten = Konten::find($id);

        //$passport->name=$request->get('name');

        $konten->judul = $request->get('judul');
        $konten->gambar = $request->get('gambar');
        $konten->isi = $request->get('isi');
        $konten->updated_at = $request->get('updated_at');

        $konten->save();

        return redirect()->route('artikel.index')->with('message', 'Data ' . $bagian . ' telah diupdate.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function ambil_data_gambar(){

        $files = glob(public_path('images/*.jpg'));

        $arr_filename = array();

        for($i = 0; $i < count($files); $i++){
            $row['filename'] = basename($files[$i]);
            $arr_filename[$row['filename']] = $row['filename'];
            //array_push($arr_filename, $filename);
        }

        $arr_filename = array_merge(array('no-pict.jpg' => '--- Silahkan Pilih Gambar ---'), $arr_filename);
        return $arr_filename;
    }
}
