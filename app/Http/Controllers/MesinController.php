<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mesin;
use Auth;


class MesinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mesin = Mesin::all();

        $subtitle = 'Index';
        $header = 'List Mesin';
        $title = 'Data Mesin';
        $user = Auth::user()->username;

        return view('admins.mesin.index')
            ->with(compact('mesin'))
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('header'))
            ->with(compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subtitle = 'Input ';
        $header = 'Input Mesin';
        $title = 'Data Mesin';
        $user = Auth::user()->username;
        $arr_filename = $this->ambil_data_gambar();


        return view('admins.mesin.addnew')
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('header'))
            ->with(compact('user'))
            ->with(compact('arr_filename'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mesin = new Mesin;

        $mesin->nama = $request->input('nama');
        $mesin->gambar = $request->input('gambar');
        $mesin->save();

        return redirect()->route('mesin.index')->with('message', 'Data mesin baru telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subtitle = 'Mesin ';
        $header = 'Edit Mesin';
        $title = 'Data Mesin';
        $user = Auth::user()->username;
        $arr_filename = $this->ambil_data_gambar();

        $mesin = Mesin::find($id);

        return view('admins.mesin.edit')
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('header'))
            ->with(compact('user'))
            ->with(compact('mesin'))
            ->with(compact('arr_filename'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mesin $mesin)
    {
        $mesin->nama = $request->get('nama');
        $mesin->gambar = $request->get('gambar');

        $mesin->save();
        return redirect()->route('mesin.index')->with('message', 'Data ' . $mesin->id . ' telah diupdate.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mesin $mesin)
    {
        $mesin->delete();
        return redirect()->route('mesin.index')->with('message', 'Data ' . $mesin->id . ' telah dihapus.');
    }

    function ambil_data_gambar()
    {
        $files = glob(public_path('/images/mesin/*.jpg'));
        $arr_filename = array();

        for ($i = 0; $i < count($files); $i++) {
            $row['filename'] = basename($files[$i]);
            $arr_filename[$row['filename']] = $row['filename'];

        }

        $arr_filename = array('' => '--- Pilih Gambar ---') + $arr_filename;

        return $arr_filename;
    }
}
