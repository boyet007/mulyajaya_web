<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Konten;
use App\Carousel;
use App\Mesin;
use App\Setting;
use App\Galeri;
use App\Kontak;
use App\Kategori;

use Auth;

class KontenController extends Controller
{

    public function index()
    {

        $konten = Konten::all();

        $carousels = Carousel::all();
        $gambar_mesin = Mesin::all();
        $title = "Utama";

        return view('webpages.utama')
            ->with(compact('konten'))
            ->with(compact('carousels'))
            ->with(compact('gambar_mesin'))
            ->with(compact('utama'))
            ->with(compact('title'));
    }

    public function baca_artikel($id)
    {

        $konten = Konten::find($id);

        $title = "Baca Artikel";

        return view('webpages.baca')
            ->with(compact('title'))
            ->with(compact('konten'));
    }

    public function galeri()
    {
        $galeri = Galeri::paginate(12);
        $title = "Galeri";

        return view('webpages.galeri')
            ->with(compact('title'))
            ->with(compact('galeri'));
    }

    public function kontak()
    {
        $title = "Kontak Kami";

        return view('webpages.kontak')
            ->with(compact('title'));
    }

    public function simpan_kontak(Request $request)
    {

        $res = $this->google_cek();

        if ($res->success) {
            //return $request->all();
            Kontak::create($request->except(['_method']));
            return back()->with('message', 'Terima kasih sudah mengisi kontak, kami akan meresponnya sesegera mungkin.');
        } else {
            /* captcha failed */
            return back()->withErrors([
                'message' => 'Isilah recaptcha dengan benar.'
            ]);
        }
    }

    public function kategori($id)
    {
        $spekat = Kategori::find($id);
        $produk = Kategori::find($id)->produk;
        $title = 'Kategori';

        return view('webpages.kategori')
            ->with(compact('spekat'))
            ->with(compact('produk'))
            ->with(compact('title'));
    }

    public function login()
    {
        $title = 'Login';
        return view('webpages.login')->with(compact('title'));
    }

    public function cek_login(Request $request)
    {
        $res = $this->google_cek();

        if ($res->success) {

            $data = array(
                'username' => $request->input('username'),
                'password' => $request->input('password')
            );

            if (Auth::attempt($data)) {
                return redirect()->route('dashboard');
            } else {
                return back()->withErrors([
                    'message' => 'Username atau Password anda salah.'
                ]);
            }
        } else {
            /* captcha failed */
            return back()->withErrors([
                'message' => 'Isilah recaptcha dengan benar.'
            ]);
        }
    }

    function google_cek()
    {
        $secret = env('NOCAPTCHA_SECRET');
        $captcha = trim($_POST['g-recaptcha-response']);
        $ip = $_SERVER['REMOTE_ADDR'];
        $url = "https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$captcha}&remoteip={$ip}";

        $options = array(
            'ssl' => array(
                'cafile' => 'cacert.pem',
                'verify_peer' => true,
                'verify_peer_name' => true,
            ),
        );
        $context = stream_context_create($options);
        $res = json_decode(file_get_contents($url, FILE_TEXT, $context));

        return $res;
    }
}
