<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use Auth;

class SettingController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
      $setting = Setting::all();

      $subtitle = 'Index';
      $header = 'List Setting';
      $title = 'Data Setting';
      $user = Auth::user()->username;

      return view('admins.setting.index')
          ->with(compact('setting'))
          ->with(compact('title'))
          ->with(compact('subtitle'))
          ->with(compact('header'))
          ->with(compact('user'));

  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $setting= Setting::find($id);

    $user = Auth::user()->username;
    $title = 'Data Setting';
    $subtitle = 'Setting';
    $header = 'Edit Setting';
    $arr_filename = $this->ambil_data_gambar();

    return view('admins.setting.edit')
    ->with(compact('title'))
    ->with(compact('subtitle'))
    ->with(compact('user'))
    ->with(compact('header'))
    ->with(compact('setting'))
    ->with(compact('arr_filename'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, Setting $setting)
  {
    $setting->nama_perusahaan = $request->get('nama_perusahaan');
    $setting->tag = $request->get('tag');
    $setting->nama_tempat = $request->get('nama_tempat');
    $setting->blok = $request->get('blok');
    $setting->nama_jalan = $request->get('nama_jalan');
    $setting->kota = $request->get('kota');
    $setting->kode_pos = $request->get('kode_pos');
    $setting->telp = $request->get('telp');
    $setting->fax = $request->get('fax');
    $setting->email = $request->get('email');
    $setting->keterangan = $request->get('keterangan');
    $setting->gambar = $request->get('gambar');

    $setting->save();

    return redirect()->route('setting.index')->with('message', 'Data setting telah diupdate.');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }

  function ambil_data_gambar(){
    $files = glob(public_path('images/*.jpg'));
    $arr_filename = array();

    for($i = 0; $i < count($files); $i++){
      $row['filename'] = basename($files[$i]);
      $arr_filename[$row['filename']] = $row['filename'];
      //array_push($arr_filename, $filename);
    }

    $arr_filename = array_merge(array('no-pict.jpg' => '--- Silahkan Pilih Gambar ---'), $arr_filename);
    return $arr_filename;
  }
}
