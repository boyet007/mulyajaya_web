<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Kontak;
use App\Setting;
use DB;


class AdminController extends Controller
{
    public function index()
    {
        $title = 'Dashboard';
        $subtitle = 'Muka';

        $kontak = DB::table('kontak')
            ->orderby('waktu', 'desc')
            ->paginate(10);

        $user = Auth::user()->username;

        return view('admins.dashboard')
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('kontak'))
            ->with(compact('user'));
    }

    public function baca_kontak(Request $request)
    {

        $id = $request->segment(2);

        $user = Auth::user()->username;

        $title = 'Dashboard';
        $subtitle = 'Email';
        $kontak = Kontak::find($id);

        return view('admins.baca-email')
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('user'))
            ->with(compact('kontak'));
    }

    public function hapus_kontak(Request $request)
    {
        $id = $request->segment(2);
        $kontak = Kontak::find($id);
        $kontak->delete();
        return back()->with('message', 'Data dengan no ID ' . $id . ' telah dihapus');
    }

    //======================== gambar artikel ===================================//

    public function manajemen_gambar_artikel()
    {
        $title = 'Dashboard';
        $subtitle = 'Upload Gambar Artikel';

        $user = Auth::user()->username;

        $list_gambar = $this->ambil_data_gambar('images/*.jpg');

        return view('admins.upload.gambar-artikel')
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('user'))
            ->with(compact('list_gambar'));
    }

    public function upload_gambar_artikel(Request $request)
    {
        $this->upload_gambar('images', $request);
    }

    public function hapus_gambar_artikel($file)
    {
       return $this->hapus_gambar('images', $file);
    }

    //======================== gambar produk ===================================//

    public function manajemen_gambar_produk()
    {
        $title = 'Dashboard';
        $subtitle = 'Upload Gambar Produk';

        $user = Auth::user()->username;

        $list_gambar = $this->ambil_data_gambar('images/produk/*.jpg');

        return view('admins.upload.gambar-produk')
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('user'))
            ->with(compact('list_gambar'));
    }

    public function upload_gambar_produk(Request $request)
    {
        $this->upload_gambar('images/produk', $request);
    }

    public function hapus_gambar_produk($file)
    {
      return $this->hapus_gambar('images/produk', $file);
    }

    //======================== gambar galeri ===================================//

    public function manajemen_gambar_galeri()
    {
        $title = 'Dashboard';
        $subtitle = 'Upload Gambar Galeri';

        $user = Auth::user()->username;

        $list_gambar = $this->ambil_data_gambar('images/galeri/*.jpg');

        return view('admins.upload.gambar-galeri')
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('user'))
            ->with(compact('list_gambar'));
    }

    public function upload_gambar_galeri(Request $request){
        $this->upload_gambar('images/galeri', $request);
    }

    public function hapus_gambar_galeri($file)
    {
        return $this->hapus_gambar('images/galeri', $file);
    }

    //======================== gambar carousel ===================================//

    public function manajemen_gambar_carousel()
    {
        $title = 'Dashboard';
        $subtitle = 'Upload Gambar Carousel';

        $user = Auth::user()->username;

        $list_gambar = $this->ambil_data_gambar('images/carousel/*.jpg');

        return view('admins.upload.gambar-carousel')
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('user'))
            ->with(compact('list_gambar'));
    }

    public function upload_gambar_carousel(Request $request){
        $this->upload_gambar('images/carousel', $request);
    }

    public function hapus_gambar_carousel($file)
    {
        return $this->hapus_gambar('images/carousel', $file);
    }

    //======================== gambar mesin ===================================//

    public function manajemen_gambar_mesin()
    {
        $title = 'Dashboard';
        $subtitle = 'Upload Gambar Mesin';

        $user = Auth::user()->username;

        $list_gambar = $this->ambil_data_gambar('images/mesin/*.jpg');

        return view('admins.upload.gambar-mesin')
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('user'))
            ->with(compact('list_gambar'));
    }

    public function upload_gambar_mesin(Request $request){
        $this->upload_gambar('images/mesin', $request);
    }

    public function hapus_gambar_mesin($file)
    {
        return $this->hapus_gambar('images/mesin', $file);
    }

    //=========================================================================//

    public function logout()
    {
        auth()->logout();

        return redirect('login');
    }

    function ambil_data_gambar($path)
    {
        $files = glob(public_path($path));
        $arr_filename = array();

        for ($i = 0; $i < count($files); $i++) {
            $row['filename'] = basename($files[$i]);
            array_push($arr_filename, $row['filename']);
        }

        return $arr_filename;
    }

    function upload_gambar($path, $request){
        $gambar = $request->file('file');
        $nama = $request->file->getClientOriginalName();

        $gambar->move($path, $nama);

        if ($gambar) {
            return response()->json($gambar, 200);
        } else {
            return response()->json('error', 400);
        }
    }

    function hapus_gambar($path, $file){
        $target = public_path($path . DIRECTORY_SEPARATOR  . $file);
        unlink($target);

        return back()->with('message', 'File ' . $file . ' telah dihapus');
    }
}
