<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Galeri;
use Auth;

class GaleriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galeri = Galeri::all();

        $subtitle = 'Index';
        $header = 'List Galeri';
        $title = 'Data Galeri';
        $user = Auth::user()->username;

        return view('admins.galeri.index')
            ->with(compact('galeri'))
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('header'))
            ->with(compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subtitle = 'Input ';
        $header = 'Input Galeri';
        $title = 'Data Galeri';
        $user = Auth::user()->username;
        $arr_filename = $this->ambil_data_gambar();

        return view('admins.galeri.addnew')
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('header'))
            ->with(compact('user'))
            ->with(compact('kategori'))
            ->with(compact('arr_filename'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $galeri = new Galeri;

        $galeri->nama = $request->input('nama');
        $galeri->path = $request->input('path');
        $galeri->keterangan = $request->input('keterangan');
        $galeri->save();

        return redirect()->route('galeri.index')->with('message', 'Data galeri baru telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subtitle = 'Galeri ';
        $header = 'Edit Galeri';
        $title = 'Data Galeri';
        $user = Auth::user()->username;
        $arr_filename = $this->ambil_data_gambar();

        $galeri = Galeri::find($id);

        return view('admins.galeri.edit')
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('header'))
            ->with(compact('user'))
            ->with(compact('galeri'))
            ->with(compact('arr_filename'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Galeri $galeri)
    {
        $galeri->nama = $request->get('nama');
        $galeri->path = $request->get('path');
        $galeri->keterangan = $request->get('keterangan');


        $galeri->save();
        return redirect()->route('galeri.index')->with('message', 'Data ' . $galeri->id . ' telah diupdate.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Galeri $galeri)
    {

        $galeri->delete();
        return redirect()->route('galeri.index')->with('message', 'Data ' . $galeri->id . ' telah dihapus.');
    }

    function ambil_data_gambar()
    {
        $files = glob(public_path('/images/galeri/*.jpg'));
        $arr_filename = array();

        for ($i = 0; $i < count($files); $i++) {
            $row['filename'] = basename($files[$i]);
            $arr_filename[$row['filename']] = $row['filename'];

        }

        $arr_filename = array(''=>'--- Pilih Gambar ---') + $arr_filename;

        //$arr_filename = array_merge(array('no-pict.jpg' => '--- Silahkan Pilih Gambar ---'), $arr_filename);

        return $arr_filename;
    }
}
