<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carousel;
use Auth;

class CarouselController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $carousel = Carousel::all();

      $subtitle = 'Index';
      $header = 'List Carousel';
      $title = 'Data Carousel';
      $user = Auth::user()->username;

      return view('admins.carousel.index')
          ->with(compact('carousel'))
          ->with(compact('title'))
          ->with(compact('subtitle'))
          ->with(compact('header'))
          ->with(compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $subtitle = 'Input ';
      $header = 'Input Carousel';
      $title = 'Data Carousel';
      $user = Auth::user()->username;
      $arr_filename = $this->ambil_data_gambar();

      return view('admins.carousel.addnew')
          ->with(compact('title'))
          ->with(compact('subtitle'))
          ->with(compact('header'))
          ->with(compact('user'))
          ->with(compact('arr_filename'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $carousel = new Carousel;

      $carousel->nama = $request->input('nama');
      $carousel->gambar = $request->input('gambar');
      $carousel->save();

      return redirect()->route('carousel.index')->with('message', 'Data carousel baru telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $subtitle = 'Carousel ';
      $header = 'Edit Carousel';
      $title = 'Data Carousel';
      $user = Auth::user()->username;
      $arr_filename = $this->ambil_data_gambar();

      $carousel = Carousel::find($id);

      return view('admins.carousel.edit')
          ->with(compact('title'))
          ->with(compact('subtitle'))
          ->with(compact('header'))
          ->with(compact('user'))
          ->with(compact('carousel'))
          ->with(compact('arr_filename'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Carousel $carousel)
    {
      $carousel->nama = $request->get('nama');
      $carousel->gambar = $request->get('gambar');

      $carousel->save();
      return redirect()->route('carousel.index')->with('message', 'Data ' . $carousel->id . ' telah diupdate.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Carousel $carousel)
    {
      $carousel->delete();
      return redirect()->route('carousel.index')->with('message', 'Data ' . $carousel->id . ' telah dihapus.');
    }

    function ambil_data_gambar()
    {
        $files = glob(public_path('/images/carousel/*.jpg'));
        $arr_filename = array();

        for ($i = 0; $i < count($files); $i++) {
            $row['filename'] = basename($files[$i]);
            $arr_filename[$row['filename']] = $row['filename'];

        }

        $arr_filename = array(''=>'--- Pilih Gambar ---') + $arr_filename;

        //$arr_filename = array_merge(array('no-pict.jpg' => '--- Silahkan Pilih Gambar ---'), $arr_filename);

        return $arr_filename;
    }
}
