<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Produk;
use App\Kategori;
use Auth;
use DB;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = DB::table('produk')
            ->join('kategori', 'produk.kat_id', '=', 'kategori.id')
            ->select('produk.*', 'kategori.nama as kategori')
            ->orderBy('id')
            ->get();

        $subtitle = 'Index';
        $header = 'List Produk';
        $title = 'Data Produk';
        $user = Auth::user()->username;

        return view('admins.produk.index')
            ->with(compact('produk'))
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('header'))
            ->with(compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subtitle = 'Input ';
        $header = 'Input Produk';
        $title = 'Data Produk';
        $user = Auth::user()->username;
        $kategori = $this->ambil_data_kategori();
        $arr_filename = $this->ambil_data_gambar();


        return view('admins.produk.addnew')
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('header'))
            ->with(compact('user'))
            ->with(compact('kategori'))
            ->with(compact('arr_filename'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produk = new Produk;

        $produk->kat_id = $request->input('kat_id');
        $produk->nama = $request->input('nama');
        $produk->gambar = $request->input('gambar');
        $produk->keterangan = $request->input('keterangan');
        $produk->save();

        return redirect()->route('produk.index')->with('message', 'Data produk baru telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subtitle = 'Input ';
        $header = 'Edit Produk';
        $title = 'Data Produk';
        $user = Auth::user()->username;
        $kategori = $this->ambil_data_kategori();
        $arr_filename = $this->ambil_data_gambar();

        $produk = Produk::find($id);


        return view('admins.produk.edit')
            ->with(compact('title'))
            ->with(compact('subtitle'))
            ->with(compact('header'))
            ->with(compact('user'))
            ->with(compact('produk'))
            ->with(compact('kategori'))
            ->with(compact('arr_filename'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, produk$produk)
    {
        $produk->kat_id = $request->get('kat_id');
        $produk->nama = $request->get('nama');
        $produk->gambar = $request->get('gambar');
        $produk->keterangan = $request->get('keterangan');


        $produk->save();
        return redirect()->route('produk.index')->with('message', 'Data ' . $produk->id . ' telah diupdate.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produk $produk)
    {
        $produk->delete();
        return redirect()->route('produk.index')->with('message', 'Data ' . $produk->id . ' telah dihapus.');
    }

    function ambil_data_kategori()
    {
        $array_kat = DB::table('kategori')->pluck('nama', 'id')->toArray();
        $par_data_kategori = array('' => '--- Pilih Kategori ---') + $array_kat;

        return $par_data_kategori;
    }

    function ambil_data_gambar()
    {
        $files = glob(public_path('/images/produk/*.jpg'));
        $arr_filename = array();

        for ($i = 0; $i < count($files); $i++) {
            $row['filename'] = basename($files[$i]);
            $arr_filename[$row['filename']] = $row['filename'];

        }

        $arr_filename = array('' => '--- Pilih Gambar ---') + $arr_filename;

        return $arr_filename;
    }
}
