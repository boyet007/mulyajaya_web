<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;
use Auth;

class KategoriController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

  public function index()
  {
    $kategori = Kategori::all();
    $subtitle = 'Index';
    $header = 'List Kategori';
    $title ='Data Kategori';
    $user =  Auth::user()->username;

    return view('admins.kategori.index')
    ->with(compact('kategori'))
    ->with(compact('title'))
    ->with(compact('subtitle'))
    ->with(compact('header'))
    ->with(compact('user'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $subtitle = 'Input ';
    $header = 'Input Kategori';
    $title = 'Data Kategori';
    $user =  Auth::user()->username;

    return view('admins.kategori.addnew')
    ->with(compact('title'))
    ->with(compact('subtitle'))
    ->with(compact('header'))
    ->with(compact('user'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {

    $kategori = new Kategori;

    $kategori->nama = $request->input('nama');
    $kategori->keterangan = $request->input('keterangan');
    $kategori->save();

    return redirect()->route('kategori.index')->with('message', 'Data kategori baru telah ditambahkan');
  }

  /**
  * Display the specified resource.
  *
  * @param  \App\kategori  $kategori
  * @return \Illuminate\Http\Response
  */
  public function show(kategori $kategori)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\kategori  $kategori
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $subtitle = 'Edit';
    $header = 'Edit Kategori';
    $title = 'Data Kategori';
    $user =  Auth::user()->username;

    $kategori = Kategori::find($id);

    return view('admins.kategori.edit')
    ->with(compact('title'))
    ->with(compact('subtitle'))
    ->with(compact('header'))
    ->with(compact('user'))
    ->with(compact('kategori'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\kategori  $kategori
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, kategori $kategori)
  {
    $kategori->nama = $request->get('nama');
    $kategori->keterangan = $request->get('keterangan');

    $kategori->save();
    return redirect()->route('kategori.index')->with('message', 'Data ' . $kategori->id . ' telah diupdate.');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\kategori  $kategori
  * @return \Illuminate\Http\Response
  */
  public function destroy(Kategori $kategori)
  {
    $kategori->delete();
    return redirect()->route('kategori.index')->with('message', 'Data ' . $kategori->id . ' telah dihapus.');
  }
}
