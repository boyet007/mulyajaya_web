<?php
/**
 * Created by PhpStorm.
 * User: Philip
 * Date: 5/8/2018
 * Time: 1:47 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Model extends Eloquent
{
    protected $guarded = [];

}