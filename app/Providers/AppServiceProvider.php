<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Kategori;
use App\Jasa;
use App\Setting;
use App\Mesin;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       Schema::defaultStringLength(191);


       // $tracker = new Tracker();
       // $tracker->url = $request->url();
       // $tracker->ip = $request->getClientIp();
       // $tracker->agent = $request->header('User-Agent');
       // $tracker->waktu = date('Y-m-d H:i:s');
       // $tracker->save();




       $kategori = Kategori::all('id', 'nama');
       view()->share(compact('kategori'));

       $jasa = Jasa::all('nama');
       view()->share(compact('jasa'));

       $workshop = Setting::find(1);
       view()->share(compact('workshop'));

       $mesin = Mesin::all();
       view()->share(compact('mesin'));

       $toko = Setting::find(2);
       view()->share(compact('toko'));




   }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
