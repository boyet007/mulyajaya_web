<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{

 protected $table = 'kategori';

 public $timestamps = false;

    public function produk(){
    	return $this->hasMany('App\Produk', 'kat_id');
    }
}
