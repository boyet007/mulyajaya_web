<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
	 protected $table = 'produk';

	 public $timestamps = false;

    public function Kategori(){
    	$this->belongsTo(Kategori::class);
    }
}
