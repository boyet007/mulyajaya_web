<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'KontenController@index');

Route::get('baca-artikel/{id}', 'KontenController@baca_artikel');

Route::get('gambar-galeri', 'KontenController@galeri');

Route::get('web-kategori/{id}', 'KontenController@kategori');

Route::get('kontak', 'KontenController@kontak');

Route::post('kontak', [
    'uses' => 'KontenController@simpan_kontak',
    'as' => 'simpan_kontak'
]);

Route::get('login', [
    'uses' => 'KontenController@login',
    'as' => 'login'
]);

Route::post('login', [
    'uses' => 'KontenController@cek_login',
    'as' => 'cek_login'
]);

Route::group(['middleware' => 'auth'], function () {

    Route::get('dashboard', [
        'uses' => 'AdminController@index',
        'as' => 'dashboard'
    ]);

    Route::get('logout', [
        'uses' => 'AdminController@logout',
        'as' => 'logout'
    ]);

    Route::get('baca-kontak/{$id}', [
        'uses' => 'AdminController@baca_kontak',
        'as' => 'baca-kontak'
    ]);

    Route::get('hapus_kontak/{id}', 'AdminController@hapus_kontak');

    Route::resource('artikel', 'ArtikelController');
    Route::resource('kategori', 'KategoriController');
    Route::resource('produk', 'ProdukController');
    Route::resource('galeri', 'GaleriController');
    Route::resource('carousel', 'CarouselController');
    Route::resource('setting', 'SettingController');
    Route::resource('mesin', 'MesinController');

    Route::get('manajemen_gambar_artikel', 'AdminController@manajemen_gambar_artikel');
    Route::post('upload_gambar_artikel', 'AdminController@upload_gambar_artikel');
    Route::get('hapus_gambar_artikel/{file}', 'AdminController@hapus_gambar_artikel');

    Route::get('manajemen_gambar_produk', 'AdminController@manajemen_gambar_produk');
    Route::post('upload_gambar_produk', 'AdminController@upload_gambar_produk');
    Route::get('hapus_gambar_produk/{file}', 'AdminController@hapus_gambar_produk');

    Route::get('manajemen_gambar_galeri', 'AdminController@manajemen_gambar_galeri');
    Route::post('upload_gambar_galeri', 'AdminController@upload_gambar_galeri');
    Route::get('hapus_gambar_galeri/{file}', 'AdminController@hapus_gambar_galeri');

    Route::get('manajemen_gambar_carousel', 'AdminController@manajemen_gambar_carousel');
    Route::post('upload_gambar_carousel', 'AdminController@upload_gambar_carousel');
    Route::get('hapus_gambar_carousel/{file}', 'AdminController@hapus_gambar_carousel');

    Route::get('manajemen_gambar_mesin', 'AdminController@manajemen_gambar_mesin');
    Route::post('upload_gambar_mesin', 'AdminController@upload_gambar_mesin');
    Route::get('hapus_gambar_mesin/{file}', 'AdminController@hapus_gambar_mesin');

});

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

//==============================Testing================================//

Route::get('/testing', function () {
    return view('testing');
});

Route::get('/testing2', function () {
    $files = File::allFiles(public_path('images/caraousel'));

    $caraousel = array();

    foreach ($files as $file) {
        array_push($caraousel, basename((string)$file));
    }

    return $caraousel[0];
});

Route::get('/testing3', function () {
    $carousels = App\Carousel::get();
    return $carousels[3]->gambar;
});

Route::get('/caraousel', function () {
    $caraousels = App\Caraousel::all();
    return $caraousels;
});
//=========================== End Testing =================================//
