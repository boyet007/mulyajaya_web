@extends('layouts.master2')
@section('title')
{{ $title }}
@endsection
@section('content')

	<div class="article">
		<h2>{{ $konten->judul }}</h2>
			<a class="group2" href="{{ URL::to('/') }}/images/{{ $konten->gambar }}"><img class="img img-rounded img-right" src="{{ URL::to('/') }}/images/{{ $konten->gambar }}"></a>
		<p>{{ $konten->isi }}</p>
		<civ class="clearfix"></civ>
		<a class="btn btn-primary mt-2" href="{{ url('/') }}">Kembali</a>
	</div>

@include('layouts.modal')
@endsection
