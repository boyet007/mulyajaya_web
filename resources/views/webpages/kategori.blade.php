@extends('layouts.master2')
@section('title')
{{ $title }}
@endsection
@section('content')

<div class="article">
	<h2>{{ $title }} : {{ $spekat->nama }}</h2>
	<h3>Deskripsi : </h3>
	<p>{{ $spekat->keterangan }}</p>
	<h3>Produk :</h3>

	@foreach($produk as $p)
		@if($loop->iteration % 2 == 0)
			<div class="produk-detail">
				<a class="group3" title="{{ $p->nama }}" href="{{ URL::to('/') }}/images/produk/{{ $p->gambar }}"><img class="img-left" src="{{ URL::to('/') }}/images/produk/{{ $p->gambar }}"></a>
				<h1>{{ $p->nama}}</h1>
				<p>{{ $p->keterangan }}
				<div class="clearfix"></div>
			</div>
		@else
			<div class="produk-detail">

				<a class="group3" title="{{ $p->nama }}" href="{{ URL::to('/') }}/images/produk/{{ $p->gambar }}"><img class="img-right" src="{{ URL::to('/') }}/images/produk/{{ $p->gambar }}"></a>
				<h1>{{ $p->nama}}</h1>
				<p>{{ $p->keterangan }}
				<div class="clearfix"></div>
			</div>
		@endif
	@endforeach

	@include('layouts.modal')

</div>
@endsection
