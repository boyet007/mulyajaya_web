@extends('layouts.master2')

@section('content')

<div class="article">
   <h2>{{ $title }}</h2>
</div>


<div class="article">
    {{ Form::open(array('id' => 'form-login', 'route' => 'cek_login','method'=>'POST')) }}

    {{ csrf_field() }}

    <div class="form-group row">
        {!! Form::label('username', 'Username',array('class' => 'col-md-4 control-label d-none d-md-block text-right')) !!}
        <div class="col-md-5">
            {!! Form::text('username', null, array('class' => 'form-control', 'placeholder' => 'Username Anda')) !!}
        </div>
    </div>
    <div class="form-group row">
        {!! Form::label('password', 'Password',array('class' => 'col-md-4 control-label d-none d-md-block text-right') ) !!}
        <div class="col-md-5">
            {!! Form::password('password',  array('class' => 'form-control', 'placeholder' => 'Password Anda')) !!}
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-5 offset-md-4">
           {!! app('captcha')->display() !!}
       </div>
       @if (count($errors))
            <div class="col-md-5 offset-md-4">      
                @foreach ($errors->all() as $error)
                    <span class="error">{{  $error }}</span>
                @endforeach
            </div>
        @endif
   </div>
<div class="Form-group row">
    <div class="col-md-5 offset-md-4"> 
       {!! Form::submit('Login', array('class' => ' btn btn-warning btn-lg')) !!}
   </div>
</div>
{{ Form::close() }}
</div>


@endsection