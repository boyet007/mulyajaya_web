@extends('layouts.master')
@section('title')
    {{ $title }}
@endsection
@section('content')

    <div class="mesin">
        @foreach($gambar_mesin as $g)
            <a class="group3" title="{{ $g->nama }}" href="{{ URL::to('/') }}/images/mesin/{{ $g->gambar }}">
                <img src="{{ URL::to('/') }}/images/mesin/{{ $g->gambar }}"></a>
        @endforeach
    </div>

    @foreach($konten as $k)
        <div class="article">
            <h2>{{ $k->judul }}</h2>
            @if($k->id % 2 == 0)
                <a class="group2" href="{{ URL::to('/') }}/images/{{ $k->gambar }}"><img
                            class="img img-rounded img-right" src="{{ URL::to('/') }}/images/{{ $k->gambar }}"></a>
            @else
                <a class="group2" href="{{ URL::to('/') }}/images/{{ $k->gambar }}"><img
                            class="img img-rounded img-left" src="{{ URL::to('/') }}/images/{{ $k->gambar }}"></a>
            @endif
            <p>{{ str_limit($k->isi, 500) }}</p>
            <civ class="clearfix"></civ>
            <a class="btn btn-primary mt-2" href="{{ url('baca-artikel', $k->id) }}">Selengkapnya</a>
        </div>
    @endforeach

    @include('layouts.modal')
@endsection
