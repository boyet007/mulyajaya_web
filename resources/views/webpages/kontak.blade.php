@extends('layouts.master2')
@section('title')
{{ $title }}
@endsection
@section('content')

<div class="article">
 <h2>{{ $title }}</h2>
</div>

<div class="article">

  @if(session()->has('message'))
  <div class="alert alert-success">
    {{ session()->get('message') }}
  </div>
  @endif

  {{ Form::open(array('id' => 'form-kontak', 'route' => 'simpan_kontak','method'=>'POST')) }}

  <input type="hidden" name="waktu" value="<?php echo date('Y-m-d H:i:s'); ?>" readonly="readonly">

  <!-- Name input-->
  <div class="form-group row">
    {!! Form::label('nama', 'Nama',array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
    <div class="col-md-7">
     {!! Form::text('nama', null, array('id' => 'name', 'class' => 'form-control', 'placeholder' => 'Nama Anda', 'required' => '')) !!}
   </div>
 </div>

 <div class="form-group row">
  {!! Form::label('perusahaan', 'Perusahaan',array('class' => 'col-md-3 d-none d-md-block text-right control-label')) !!}
  <div class="col-md-7">
    {!! Form::text('perusahaan', null, array('id' => 'company', 'class' => 'form-control', 'placeholder' => 'Perusahaan Anda', 'required' => '')) !!}
  </div>
</div>

<div class="form-group row">
  {!! Form::label('email', 'Email',array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
  <div class="col-md-7">
    {!! Form::email('email', null, array('id' => 'email', 'class' => 'form-control', 'placeholder' => 'Email Anda', 'required' => '')) !!}
  </div>
</div>

<div class="form-group row">
  {!! Form::label('pesan', 'Pesan',array('class' => 'col-md-3 d-none d-md-block text-right control-label')) !!}
  <div class="col-md-7">
    {!! Form::textarea('pesan', null, array('id' => 'message', 'class' => 'form-control', 'placeholder' => 'Silahkan tulis pesan anda disini...', 'required' => '', 'rows' => '5')) !!}
  </div>
</div>

<div class="form-group row">
  <div class="col-md-7 offset-md-3">
   {!! app('captcha')->display() !!}
 </div>
</div>

<div class="form-group row">
  <div class="col-md-7 offset-md-3">
    <button type="submit" class="btn btn-warning btn-lg">Submit</button>
  </div>
</div>

@if (count($errors))
<div class="form-group row">
<div class="col-md-7 offset-md-3">      
  @foreach ($errors->all() as $error)
  <span class="error">{{  $error }}</span>
  @endforeach
</div>
</div>
@endif


{{ Form::close() }}

</div>


@endsection