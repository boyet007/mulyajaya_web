@extends('layouts.master2')
@section('title')
    {{ $title }}
@endsection
@section('content')


    <div class="col-md-12">
        <div class="article">
            <h2>{{ $title }}</h2>
        </div>
    </div>

    <div class="col-md-12">
        <div class="article">
            <div class="row text-center text-lg-left">
                @foreach ($galeri as $g)
                    <div class=" col-md-4 col-xs-6">
                        <a class="group2" title="{{ $g->nama }}" href="{{ URL::to('/') }}/images/galeri/{{ $g->path }}">
                            <img class="img-fluid img-thumbnail" src="{{ URL::to('/') }}/images/galeri/{{ $g->path }}"
                                 alt="">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="col-md-12">
    {{ $galeri->links() }}
    </div>

@endsection
