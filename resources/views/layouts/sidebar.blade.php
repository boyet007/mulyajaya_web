
<div class="article">
    <h2>Peta</h2>
    <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d350.61208364153345!2d106.9462691395255!3d-6.167847469878084!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e698b51b6c9d759%3A0x9355fc474d596dd2!2sCV.+Mulya+Jaya!5e0!3m2!1sen!2sid!4v1526114609332" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe></p>
</div>

<div class="article">
    <h2>Pengerjaan</h2>

    <ul>
    @foreach($jasa as $j)
        <li>{{ $j->nama }}</li>
    @endforeach
</ul>
</div>

<div class="article contact">
    <h2>Company Profile</h2>
    <h3>{{ $workshop->nama_perusahaan }}</h3>

    <img class="profile-pic" src="{{ asset('images/' . $workshop->gambar) }}" />

    <br>
    <table class="bullet">
      <tr><td></td><td><h4><b>Workshop</b></h4></td></tr>
        <tr>
            <td class="td1"><span class="fa fa-home"></span> </td>
            <td class="td2">{{ $workshop->nama_tempat }}</td>
        </tr>
        <tr>
            <td class="td1"></span> </td>
            <td class="td2">{{ $workshop->blok }}</td>
        </tr>
        <tr>
            <td class="td1"></span> </td>
            <td class="td2">{{ $workshop->nama_jalan }}</td>
        </tr>
        <tr>
            <td class="td1"></td>
            <td class="td2">{{ $workshop->kota}} - {{ $workshop->kode_pos }}</td>
        </tr>
        <tr>
            <td class="td1"><span class="fa fa-phone"></span></td>
            <td class="td2">{{$workshop->telp }}</td>
        </tr>
        <tr>
            <td class="td1"><span class="fa fa-fax"></span></td>
            <td class="td2">{{$workshop->fax }}</td>
        </tr>
        <tr>
            <td class="td1"><span class="fa fa-envelope"></span></td>
            <td class="td2"><a href="mailto:{{ $workshop->email }}}"><b>{{ $workshop->email }}</b></a></td>
        </tr>
    </table>

    <br>
    <br>

    <table class="bullet">
      <tr><td></td><td><h4><b>Galeri</b></h4></td></tr>
        <tr>
            <td class="td1"><span class="fa fa-home"></span> </td>
            <td class="td2">{{ $toko->nama_tempat }}</td>
        </tr>
        <tr>
            <td class="td1"></span> </td>
            <td class="td2">{{ $toko->blok }}</td>
        </tr>
        <tr>
            <td class="td1"></span> </td>
            <td class="td2">{{ $toko->nama_jalan }}</td>
        </tr>
        <tr>
            <td class="td1"></td>
            <td class="td2">{{ $toko->kota}} - {{ $toko->kode_pos }}</td>
        </tr>
        <tr>
            <td class="td1"><span class="fa fa-phone"></span></td>
            <td class="td2">{{$toko->telp }}</td>
        </tr>
    </table>
</div>
