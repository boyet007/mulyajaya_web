<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon-96x96.png') }}">
    <title>@yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Jquery UI -->
    <Link href="{{ asset('vendor/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <!-- jQuery -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <!-- jQuery UI -->
    <script src="{{ asset('vendor/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Gallery Colorbox -->
    <script src="{{ asset('vendor/colorbox/jquery.colorbox-min.js')}}"></script>
    <!-- Jquery Validation -->
    <script src="{{ asset('vendor/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <!-- google recaptcha -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- Custom Javascript -->
    <script src="{{ asset('js/script.js') }}"></script>
    <!-- Colorbox CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/colorbox/example1/colorbox.css') }}" />
    <!-- Custom CSS -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
    @include('layouts.navbar')
    <div class="container">
        <div class="row">
            <div class="batas col-lg-8">
                    @yield('content')
            </div>           
            <div class="batas col-lg-4">
                @include('layouts.sidebar')
            </div>
        </div>
    </div>

    @include('layouts.footer')

    <!--<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
    <script src="https://npmcdn.com/bootstrap@4.0.0-alpha.5/dist/js/bootstrap.min.js"></script> -->

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
</body>
</html>
