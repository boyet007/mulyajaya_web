<nav class="navbar navbar-expand-lg navbar-light bg-dark">
  <a class="navbar-brand" href="#">{{ $workshop->nama_perusahaan }}</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="{{ url('/') }}">Utama <span class="sr-only">(current)</span></a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Kategori Produk
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">

          @foreach ($kategori as $k)
            <a class="dropdown-item" href="{{ url('web-kategori', [$k->id]) }}">{{$k -> nama }}</a>

          @endforeach
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('gambar-galeri') }}">Galeri</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('kontak') }}">Kontak Kami</a>
      </li>
    </ul>
    <div class="my-2 my-lg-0">
      <a class="btn btn-outline-warning my-2 my-sm-0" href="{{ url('login') }}">Login</a>
    </div>
  </div>
</nav>
