@if (count($errors))
<div class="col-md-9">  		
  
   @foreach ($errors->all() as $error)
   <span class="error">{{  $error }}</span>
   @endforeach
 
</div>
@endif
