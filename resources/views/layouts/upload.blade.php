<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <!-- Bootstrap core CSS-->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Colorbox CSS -->
    <link href="{{ asset('vendor/colorbox/example2/colorbox.css') }}" rel="stylesheet"  />
    <!-- Dropzone -->
    {{--<link href="{{ asset('vendor/dropzone/min/basic.min.css') }}" rel="stylesheet" type="text/css">--}}
    <link href="{{ asset('vendor/dropzone/min/dropzone.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="{{ asset('vendor/sb-admin/css/sb-admin.min.css') }}" rel="stylesheet">
    <!-- Custom styles Mulya Jaya -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="{{ route('dashboard')  }}">CV. Mulya Jaya</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
                <a class="nav-link" href="{{ route('artikel.index') }}">
                    <i class="fa fa-fw fa-edit"></i>
                    <span class="nav-link-text">Artikel</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
                <a class="nav-link" href="{{ route('kategori.index') }}">
                    <i class="fa fa-fw fa-edit"></i>
                    <span class="nav-link-text">Kategori</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
                <a class="nav-link" href="{{ route('produk.index') }}">
                    <i class="fa fa-fw fa-edit"></i>
                    <span class="nav-link-text">Produk</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
                <a class="nav-link" href="{{ route('galeri.index') }}">
                    <i class="fa fa-fw fa-edit"></i>
                    <span class="nav-link-text">Galeri</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
                <a class="nav-link" href="{{ route('carousel.index') }}">
                    <i class="fa fa-fw fa-edit"></i>
                    <span class="nav-link-text">Carousel</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
                <a class="nav-link" href="{{ route('mesin.index') }}">
                    <i class="fa fa-fw fa-edit"></i>
                    <span class="nav-link-text">Mesin</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
                    <i class="fa fa-fw fa-upload"></i>
                    <span class="nav-link-text">Upload Gambar</span>
                </a>
                <ul class="sidenav-second-level collapse" id="collapseComponents">
                    <li>
                        <a href="{{ url('manajemen_gambar_artikel')  }}">Artikel</a>
                    </li>
                    <li>
                        <a href="{{ url('manajemen_gambar_produk')  }}">Produk</a>
                    </li>
                    <li>
                        <a href="{{ url('manajemen_gambar_galeri')  }}">Galeri</a>
                    </li>
                    <li>
                        <a href="{{ url('manajemen_gambar_carousel')  }}">Carousel</a>
                    </li>
                    <li>
                        <a href="{{ url('manajemen_gambar_mesin')  }}">Mesin</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
                <a class="nav-link" href="{{ route('setting.edit', '1') }}">
                    <i class="fa fa-fw fa-gears"></i>
                    <span class="nav-link-text">Setting</span>
                </a>
            </li>

        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-right" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
            <li class="nav-item">
                <a class="nav-link">Tanggal : <span id="tanggal"></span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link">Jam : <span id="waktu"></span></a>
            </li>

        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a href="{{ route('dashboard') }}" class="nav-link">Selamat Datang, {{ $user }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
                    <i class="fa fa-fw fa-sign-out"></i>Keluar</a>
            </li>
        </ul>
    </div>
</nav>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}">@yield('title')</a>
            </li>
            <li class="breadcrumb-item active">@yield('subtitle')</li>
        </ol>
        <div class="row">

                @yield('content')

        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->
        <footer class="sticky-footer">
            <div class="container">
                <div class="text-center">
                    <small>Created By Philip Loa Pleyto 2018</small>
                </div>
            </div>
        </footer>
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fa fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Yakin ingin keluar?</h5>
                    </div>
                    <div class="modal-body"><p>Pilih tombol keluar jika ingin mengakhiri session admin.</p></div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <a class="btn btn-primary" href="{{ url('logout') }}">Keluar</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <!-- Core plugin JavaScript-->
        <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
        <!-- Custom scripts for all pages-->
        <script src="{{ asset('vendor/sb-admin/js/sb-admin.min.js') }}"></script>
        <!-- Moment javascript -->
        <script src="{{ asset('vendor/moment/moment.js') }}"></script>
        <!-- Gallery Colorbox -->
        <script src="{{ asset('vendor/colorbox/jquery.colorbox-min.js')}}"></script>
        <!-- Dropzone -->
        <script src="{{ asset('vendor/dropzone/min/dropzone.min.js') }}"></script>
        <!-- Jquery Validate -->
        <script src="{{ asset('vendor/jquery-validation/dist/jquery.validate.min.js') }}"></script>
        <!-- Custom scripts for CV Mulya Jaya-->
        <script src="{{ asset('js/script-admin.js') }}"></script>
    </div>
</div>
</body>

</html>
