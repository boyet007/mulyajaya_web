<div id="carouselIndicators" class="carousel slide" >
  <ul class="carousel-indicators">
    @foreach ($carousels as $carousel)
      @if ($carousel->id === 1)
      <li data-target="#carouselIndicators" data-slide-to="{{ $carousel->id - 1 }}" class="active"></li>
      @else
      <li data-target="#carouselIndicators" data-slide-to="{{ $carousel->id - 1}}"></li>
      @endif
    @endforeach
  </ul>
  <div class="carousel-inner">
    @foreach ($carousels as $carousel)
      @if($carousel->id === 1)
        <div class="carousel-item active">
          <img class="d-block w-100" src="{{ url('/images/carousel/' . $carousel->gambar) }}">
          <div class="carousel-caption d-none d-md-block">
            <h5>{{ $carousel->nama }}</h5>  
          </div>
        </div>
      @else
        <div class="carousel-item">
          <img class="d-block w-100" src="{{ url('/images/carousel/' . $carousel->gambar) }}">
          <div class="carousel-caption d-none d-md-block">
            <h5>{{ $carousel->nama }}</h5>  
          </div>
        </div>
      @endif
    @endforeach
  </div>
  <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>