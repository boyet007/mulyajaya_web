@extends('layouts.admin')

@section('title')
{{ $title }}
@endsection

@section('subtitle')
{{ $subtitle }}
@endsection

@section('content')

<h1><b>{{ $header }}</b></h1>
<hr>
@if(session()->has('message'))
<div class="alert alert-success">
  {{ session()->get('message') }}
</div>
@endif
<div>

  {{ Form::model($kategori, array('id' => 'form-kategori', 'route' => array('kategori.update', $kategori->id),  'method' => 'PUT')) }}
  <input type="hidden" name="id" value="{{ $kategori->id }}" >

  <div class="form-group row">
   {!! Form::label('nama', 'Nama',array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
   <div class="col-md-5">
    {!! Form::text('nama', $kategori->nama, array('id' => 'nama', 'class' => 'form-control', 'placeholder' => 'Nama Kategori', 'required' => '')) !!}
  </div>
</div>

  <div class="form-group row">
    {!! Form::label('keterangan', 'Keterangan',array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
    <div class="col-md-5">
      {{ Form::textarea('keterangan', $kategori->keterangan,  array( 'id' =>'keterangan', 'class' => 'form-control', 'placeholder' => 'Ket Kategori', 'required' => '')) }}
    </div>
  </div>

  <div class="form-group row">
    <div class="offset-md-3 col-md-5">
      {!! Form::submit('Update', array('class' => 'btn btn-primary  control-label')) !!}
      {!! Form::reset('Batal', array('id' => 'reset', 'class' => 'btn btn-danger  control-label')) !!}
    </div>
  </div>

 </div>
{{ Form::close() }}
</div>
@endsection
