@extends('layouts.admin')

@section('title')
{{ $title }}
@endsection

@section('subtitle')
{{ $subtitle }}
@endsection

@section('content')


<h1><b>{{ $header }}</b></h1>
<hr>
@if(session()->has('message'))
<div class="alert alert-success">
	{{ session()->get('message') }}
</div>
@endif
<div class="tambah">
  <a href="{{ route('kategori.create') }}" class="btn btn-primary">Tambah Baru</a>
</div>
<div class="table-responsive">
	<table class="table table-bordered" id="dataTable" width="100%">
		<thead>
			<th>ID</th>
			<th>Nama</th>
			<th>Keterangan</th>
			<th width="8%">Aksi</th>
		</thead>
		<tbody>

      @foreach($kategori as $k)

			<tr>
				<td>{{ $k->id }}</td>
				<td>{{ $k->nama }}</td>
				<td>{{ str_limit($k->keterangan, 300) }}</td>
				<td><a href="{{ route('kategori.edit', $k->id) }}"><i class="fa fa-edit"></i></a>&nbsp;

  			  {!! Form::open(['method' => 'POST','route' => ['kategori.destroy', $k->id],'style'=>'display:inline']) !!}
					{{ csrf_field() }}

					@method('DELETE')
						<button class="no-btn btn" type="submit"><i class="fa fa-trash"></i></button>
					{{ Form::close() }}
				</td>

			</tr>

      @endforeach

		</tbody>
	</table>
</div>

@endsection
