@extends('layouts.admin')

@section('title')
    {{ $title }}
@endsection

@section('subtitle')
    {{ $subtitle }}
@endsection

@section('content')

    <h1><b>{{ $header }}</b></h1>
    <hr>
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif

    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%">
            <thead>
            <th>ID</th>
            <th>Nama Perusahaan</th>
            <th>Tag</th>
            <th>Nama Tempat</th>
            <th>Blok</th>
            <th>Nama Jalan</th>
            <th>Kota</th>
            <th>Kode Pos</th>
            <th>Telpon</th>
            <th>Fax</th>
            <th>Email</th>
            <th>Keterangan</th>
            <th>Gambar</th>
            <th>Aksi</th>

            </thead>
            <tbody>
            @foreach($setting as $s)
                <tr>
                    <td>{{ $s->id }}</td>
                    <td>{{ $s->nama_perusahaan }}</td>
                    <td>{{ $s->tag }}</td>
                    <td>{{ $s->nama_tempat }}</td>
                    <td>{{ $s->blok }}</td>
                    <td>{{ $s->nama_jalan }}</td>
                    <td>{{ $s->kota }}</td>
                    <td>{{ $s->kode_pos }}</td>
                    <td>{{ $s->telp }}</td>
                    <td>{{ $s->fax }}</td>
                    <td>{{ $s->email }}</td>
                    <td>{{ $s->keterangan }}</td>
                    <td>{{ $s->gambar }}</td>

                    <td><a href="{{ route('setting.edit', $s->id) }}"><i class="fa fa-edit"></i></a>&nbsp;
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
