@extends('layouts.admin')

@section('title')
    {{ $title }}
@endsection

@section('subtitle')
    {{ $subtitle }}
@endsection

@section('content')

    <h1><b>{{ $header }}</b></h1>
    <hr>
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif


    <div class="tab-content tab-top-30" id="myTabContent">
        <div class="tab-pane fade show active" id="workshop" role="tabpanel" aria-labelledby="home-tab">
            {{ Form::model($setting, array('class' => 'form-setting', 'route' => array('setting.update', $workshop->id),  'method' => 'PUT')) }}

            <input type="hidden" name="id" value="{{ $setting->id }}">

            <div class="form-group row">
                {!! Form::label('nama_perusahaan', 'Nama Perusahaan', array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
                <div class="col-md-5">
                    {!! Form::text('nama_perusahaan', $setting->nama_perusahaan, array('class' => 'form-control', 'placeholder' => 'Masukkan nama perusahaan', 'required' => '')) !!}
                </div>
            </div>

            <div class="form-group row">
                {!! Form::label('tag', 'Tag', array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
                <div class="col-md-5">
                    {!! Form::text('tag', $setting->tag, array('class' => 'form-control', 'placeholder' => 'Masukkan tag perusahaan', 'required' => '')) !!}
                </div>
            </div>

            <div class="form-group row">
                {!! Form::label('nama_tempat', 'Nama Tempat', array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
                <div class="col-md-5">
                    {!! Form::text('nama_tempat', $setting->nama_tempat, array('class' => 'form-control', 'placeholder' => 'Masukkan nama tempat', 'required' => '')) !!}
                </div>
            </div>

            <div class="form-group row">
                {!! Form::label('blok', 'Blok', array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
                <div class="col-md-5">
                    {!! Form::text('blok', $setting->blok, array('class' => 'form-control', 'placeholder' => 'Masukkan blok', 'required' => '')) !!}
                </div>
            </div>

            <div class="form-group row">
                {!! Form::label('nama_jalan', 'Jalan',array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
                <div class="col-md-5">
                    {!! Form::text('nama_jalan', $setting->nama_jalan, array('class' => 'form-control', 'placeholder' => 'Masukkan nama jalan', 'required' => '')) !!}
                </div>
            </div>

            <div class="form-group row">
                {!! Form::label('kota', 'Kota',array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
                <div class="col-md-5">
                    {!! Form::text('kota', $setting->Kota, array('class' => 'form-control', 'placeholder' => 'Masukkan nama kota', 'required' => '')) !!}
                </div>
            </div>

            <div class="form-group row">
                {!! Form::label('kode_pos', 'Kode Pos',array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
                <div class="col-md-5">
                    {!! Form::text('kode_pos', $setting->kode_pos, array('class' => 'form-control', 'placeholder' => 'Masukkan kode pos', 'required' => '')) !!}
                </div>
            </div>

            <div class="form-group row">
                {!! Form::label('telp', 'Telepon',array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
                <div class="col-md-5">
                    {!! Form::text('telp', $setting->telp, array('class' => 'form-control', 'placeholder' => 'Masukkan nomor telepon', 'required' => '')) !!}
                </div>
            </div>

            <div class="form-group row">
                {!! Form::label('fax', 'Fax',array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
                <div class="col-md-5">
                    {!! Form::text('fax', $setting->fax, array('class' => 'form-control', 'placeholder' => 'Masukkan nomor fax', 'required' => '')) !!}
                </div>
            </div>

            <div class="form-group row">
                {!! Form::label('email', 'Email',array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
                <div class="col-md-5">
                    {!! Form::text('email', $setting->email, array('class' => 'form-control', 'placeholder' => 'Masukkan alamat email', 'required' => '')) !!}
                </div>
            </div>

            <div class="form-group row">
                {!! Form::label('keterangan', 'Keterangan',array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
                <div class="col-md-5">
                    {!! Form::textarea('keterangan', $setting->keterangan, array('id' => 'nama', 'class' => 'form-control', 'placeholder' => 'Masukkan keterangan', 'required' => '')) !!}
                </div>
            </div>

            <div class="form-group row">
                {!! Form::label('gambar', 'Gambar',array('class' => 'data-path col-md-3 text-right d-none d-md-block control-label')) !!}
                <div class="col-md-5">
                    {{ Form::select('gambar', $arr_filename, $setting->gambar,  array('id' => 'gambar-artikel-src', 'class' => 'form-control', 'required' => '')) }}
                    <img src="{{ URL::to('/') }}/images/{{ $setting->gambar }}" id="gambar-artikel"
                         class="img-fluid" alt="Responsive image">
                </div>
                <div class="offset-md-3 col-md-5">
                </div>
            </div>

            <div class="form-group row">
                <div class="offset-md-3 col-md-5">
                    {!! Form::submit('Update', array('class' => 'btn btn-primary  control-label')) !!}
                    {!! Form::reset('Batal', array('id' => 'reset', 'class' => 'btn btn-danger  control-label')) !!}
                </div>
            </div>

            {{ Form::close() }}


        </div>

    </div>
@endsection
