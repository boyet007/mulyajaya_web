@extends('layouts.admin')

@section('title')
    {{ $title }}
@endsection

@section('subtitle')
    {{ $subtitle }}
@endsection

@section('content')

    <h1><b>{{ $header }}</b></h1>
    <hr>
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div>

        {{ Form::model($galeri, array('id' => 'form-galeri', 'route' => array('galeri.update', $galeri->id),  'method' => 'PUT')) }}
        <input type="hidden" name="id" value="{{ $galeri->id }}" >

        <div class="form-group row">
            {!! Form::label('nama', 'Nama', array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
            <div class="col-md-5">
                {!! Form::text('nama', $galeri->nama, array('class' => 'form-control', 'placeholder' => 'Nama Galeri', 'required' => '')) !!}
            </div>
        </div>

        <div class="form-group row">
            {!! Form::label('path', 'Path',array('class' => 'data-path col-md-3 text-right d-none d-md-block control-label')) !!}
            <div class="col-md-5">
                {{ Form::select('path', $arr_filename, $galeri->path,  array('id' => 'gambar-galeri-src', 'class' => 'form-control', 'required' => '')) }}
                <img src="{{ URL::to('/') }}/images/galeri/{{ $galeri->path }}" id="gambar-galeri" class="img-fluid" alt="Responsive image">
            </div>
            <div class="offset-md-3 col-md-5">
            </div>
        </div>

        <div class="form-group row">
            {!! Form::label('keterangan', 'Keterangan',array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
            <div class="col-md-5">
                {!! Form::textarea('keterangan', $galeri->keterangan, array('id' => 'keterangan', 'class' => 'form-control', 'placeholder' => 'Keterangan', 'required' => '')) !!}
            </div>
        </div>

        <div class="form-group row">
            <div class="offset-md-3 col-md-5">
                {!! Form::submit('Update', array('class' => 'btn btn-primary  control-label')) !!}
                {!! Form::reset('Batal', array('id' => 'reset', 'class' => 'btn btn-danger  control-label')) !!}
            </div>
        </div>

        {{ Form::close() }}
    </div>
@endsection
