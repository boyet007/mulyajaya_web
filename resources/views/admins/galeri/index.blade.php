@extends('layouts.admin')

@section('title')
    {{ $title }}
@endsection

@section('subtitle')
    {{ $subtitle }}
@endsection

@section('content')


    <h1><b>{{ $header }}</b></h1>
    <hr>
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="tambah">
        <a href="{{ route('galeri.create') }}" class="btn btn-primary">Tambah Baru</a>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%">
            <thead>
            <th width="10">ID</th>
            <th width="25%">Nama</th>
            <th width="15">Path</th>
            <th width="40">Keterangan</th>
            <th width="10%">Aksi</th>
            </thead>
            <tbody>

            @foreach($galeri as $g)

                <tr>
                    <td>{{ $g->id }}</td>
                    <td>{{ $g->nama }}</td>
                    <td><img style="width:40%" src="{{ URL::to('/') }}/images/galeri/{{ $g->path }}"></td>
                    <td>{{ str_limit($g->keterangan, 300) }}</td>
                    <td><a href="{{ route('galeri.edit', $g->id) }}"><i class="fa fa-edit"></i></a>&nbsp;

                        {!! Form::open(['method' => 'POST','route' => ['galeri.destroy', $g->id],'style'=>'display:inline']) !!}
                        {{ csrf_field() }}

                        @method('DELETE')
                        <button class="no-btn btn" type="submit"><i class="fa fa-trash"></i></button>
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>

@endsection
