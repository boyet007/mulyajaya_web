@extends('layouts.admin')

@section('title')
    {{ $title }}
@endsection

@section('subtitle')
    {{ $subtitle }}
@endsection

@section('content')


    <h1><b>{{ $header }}</b></h1>
    <hr>
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="tambah">
        <a href="{{ route('mesin.create') }}" class="btn btn-primary">Tambah Baru</a>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable">
            <thead>
            <th width="5%">ID</th>
            <th width="30%">Nama</th>
            <th width="20%">Gambar</th>
            <th width="8%">Aksi</th>
            </thead>
            <tbody>

            @foreach($mesin as $m)

                <tr>
                    <td>{{ $m->id }}</td>
                    <td>{{ $m->nama }}</td>
                    <td><img style="width:50%" src="{{ URL::to('/') }}/images/mesin/{{ $m->gambar }}"></td>
                    <td><a href="{{ route('mesin.edit', $m->id) }}"><i class="fa fa-edit"></i></a>&nbsp;

                        {!! Form::open(['method' => 'POST','route' => ['mesin.destroy', $m->id],'style'=>'display:inline']) !!}
                        {{ csrf_field() }}

                        @method('DELETE')
                        <button class="no-btn btn" type="submit"><i class="fa fa-trash"></i></button>
                        {{ Form::close() }}
                    </td>

                </tr>

            @endforeach

            </tbody>
        </table>
    </div>

@endsection
