@extends('layouts.upload')

@section('title')
    {{ $title }}
@endsection

@section('subtitle')
    {{ $subtitle }}
@endsection

@section('content')
    <div class="col-md-12">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div>
            <form action="{{ url('upload_gambar_carousel') }}" id="addImages" class="dropzone" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="dz-message">
                    <b>Taruhlah file disini</b>
                    <br>
                    <span class="note">
                        Tekan dan tahan file gambar lalu masukkan ke kotak ini..<br>
                        Setelah itu tekan F5 untuk refresh...
            </span>
                </div>
            </form>
        </div>
    </div>
    @foreach ($list_gambar as $key => $value )
        <div class=" col-md-3 col-xs-6">
            <a class="group2" title="{{ $value }}" href="{{ URL::to('/') }}/images/carousel/{{ $value }}">
                <img class="img-fluid img-thumbnail" src="{{ URL::to('/') }}/images/carousel/{{ $value }}" alt="">
                <a href="{{ url('hapus_gambar_carousel/' . $value ) }}"class="btn btn-warning btn-block">Hapus</a>
            </a>
        </div>
    @endforeach
@endsection
