@extends('layouts.admin')

@section('title')
{{ $title }}
@endsection

@section('subtitle')
{{ $subtitle }}
@endsection

@section('content')


<h1><b>Edit Artikel</b></h1>
<hr>
@if(session()->has('message'))
<div class="alert alert-success">
  {{ session()->get('message') }}
</div>
@endif

<div>

  {{ Form::model($artikel, array('id' => 'form-artikel', 'route' => array('artikel.update', $artikel->id), 'method' => 'PUT')) }}

 <input type="hidden" name="id" value="{{ $artikel->id }}" >
 <input type="hidden" name="updated_at" value="<?php echo date('Y-m-d H:i:s'); ?>">

 <div class="form-group row">
  {!! Form::label('bagian', 'Bagian', array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
  <div class="col-md-5">
   {!! Form::text('bagian', $artikel->bagian, array('class' => 'form-control', 'placeholder' => 'Nama Anda', 'required' => '', 'readonly' => 'true')) !!}
 </div>
</div>

 <div class="form-group row">
  {!! Form::label('judul', 'Judul',array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
  <div class="col-md-5">
   {!! Form::text('judul', $artikel->judul, array('id' => 'judul', 'class' => 'form-control', 'placeholder' => 'Judul Artikel', 'required' => '')) !!}
 </div>
</div>

<div class="form-group row">
  {!! Form::label('gambar', 'Gambar',array('class' => 'data-gambar col-md-3 text-right d-none d-md-block control-label')) !!}
  <div class="col-md-5">
    {{ Form::select('gambar', $arr_filename, $artikel->gambar,  array('id' => 'gambar-artikel-src', 'class' => 'form-control', 'required' => '')) }}
      <img src="{{ URL::to('/') }}/images/{{ $artikel->gambar }}" id="gambar-artikel" class="img-fluid" alt="Responsive image">
  </div>
  <div class="offset-md-3 col-md-5">

  </div>
</div>

<div class="form-group row">
  {!! Form::label('isi', 'Isi',array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
  <div class="col-md-5">
    {{ Form::textarea('isi', $artikel->isi,  array('id' =>'isi', 'class' => 'form-control', 'placeholder' => 'Isi Artikel', 'required' => '')) }}
  </div>
</div>

<div class="form-group row">
  <div class="offset-md-3 col-md-5">
    {!! Form::submit('Update', array('class' => 'btn btn-primary  control-label')) !!}
    {!! Form::reset('Batal', array('id' => 'reset', 'class' => 'btn btn-danger  control-label')) !!}
  </div>
</div>


{{ Form::close() }}
</div>



@endsection
