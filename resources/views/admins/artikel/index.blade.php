@extends('layouts.admin')

@section('title')
{{ $title }}
@endsection

@section('subtitle')
{{ $subtitle }}
@endsection

@section('content')


<h1><b>List Artikel</b></h1>
<hr>
@if(session()->has('message'))
<div class="alert alert-success">
	{{ session()->get('message') }}
</div>
@endif

<div class="table-responsive">
	<table class="table table-bordered" id="dataTable" width="100%">
		<thead>
			<th>ID</th>
			<th style="width:15%">Bagian</th>
			<th style="width:15%">Judul</th>
			<th style="width:15%">Gambar</th>
			<th style="width:30%">Isi</th>
			<th style="width:15%">Tanggal</th>
			<th>Aksi</th>
		</thead>
		<tbody>
			
			<tr>
				<td>{{ $artikel_1->id }}</td>
				<td>{{ $artikel_1->bagian }}</td>
				<td>{{ $artikel_1->judul }}</td>
				<td>{{ $artikel_1->gambar }}</td>
				<td>{{ str_limit($artikel_1->isi, 300) }}</td>
				<td>{{ date('d-m-Y', strtotime($artikel_1->created_at)) }}</td>
				<td><a href="{{ route('artikel.edit', $artikel_1->id) }}"><i class="fa fa-edit"></i></a></td>
			</tr>

			<tr>
				<td>{{ $artikel_2->id }}</td>
				<td>{{ $artikel_2->bagian }}</td>
				<td>{{ $artikel_2->judul }}</td>
				<td>{{ $artikel_2->gambar }}</td>
				<td>{{ str_limit($artikel_2->isi, 300) }}</td>
				<td>{{ date('d-m-Y', strtotime($artikel_2->created_at)) }}</td>
				<td><a href="{{ route('artikel.edit', $artikel_2->id) }}"><i class="fa fa-edit"></i></a></td>
			</tr>

			<tr>
				<td>{{ $artikel_3->id }}</td>
				<td>{{ $artikel_3->bagian }}</td>
				<td>{{ $artikel_3->judul }}</td>
				<td>{{ $artikel_3->gambar }}</td>
				<td>{{ str_limit($artikel_3->isi, 300) }}</td>
				<td>{{ date('d-m-Y', strtotime($artikel_3->created_at)) }}</td>
				<td><a href="{{ route('artikel.edit', $artikel_3->id) }}"><i class="fa fa-edit"></i></a></td>
			</tr>
		
		</tbody>
	</table>
</div>

@endsection