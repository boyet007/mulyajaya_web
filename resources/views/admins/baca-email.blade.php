@extends('layouts.admin')

@section('title')
{{ $title }}
@endsection

@section('subtitle')
{{ $subtitle }}
@endsection

@section('content')

<h1><b>Baca Email</b></h1>
<hr>
<div>
  <table id="email-header">
    <tr>
      <td width="150px"><b>From</b></td>
      <td>:</td>
      <td>{{ $kontak->nama }}</td>
    </tr>
    <tr>
      <td width="150px"><b>Company</b></td>
      <td> : </td>
      <td>{{ $kontak->perusahaan }}</td>
    </tr>
    <tr>
      <td width="150px"><b>Email</b></td>
      <td> : </td>
      <td>{{ $kontak->email }}</td>
    </tr>
    <tr>
      <td width="150px"><b>Waktu</b></td>
      <td> : </td>
      <td>{{ date('d-m-Y h:i', strtotime($kontak->waktu)) }}</td>
    </tr>
  </table>
  <hr>
  <p>{{ $kontak->pesan }}</p>

  <a class="btn btn-primary mt-2" href="{{ route('dashboard') }}">Kembali</a>
</div>


@endsection
