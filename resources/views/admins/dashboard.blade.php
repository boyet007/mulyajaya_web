@extends('layouts.admin')

@section('title')
{{ $title }}
@endsection

@section('subtitle')
{{ $subtitle }}
@endsection

@section('content')


<h1><b>List Email</b></h1>
<hr>
@if(session()->has('message'))
<div class="alert alert-success">
	{{ session()->get('message') }}
</div>
@endif

<div class="table-responsive">
	<table class="table table-bordered" id="dataTable" width="100%">
		<thead>
			<th>ID</th>
			<th>Nama</th>
			<th>Perusahaan</th>
			<th>Email</th>
			<th style="width:30%">Pesan</th>
			<th>Waktu</th>
			<th style="width:8%">Aksi</th>
		</thead>
		<tbody>
			@foreach($kontak as $k)
			<tr>
				<td>{{ $k->id }}</td>
				<td>{{ $k->nama }}</td>
				<td>{{ $k->perusahaan }}</td>
				<td>{{ $k->email }}</td>
				<td>{{ str_limit($k->pesan, 200) }}</td>
				<td>{{ date('d-m-Y h:i', strtotime($k->waktu)) }}</td>
				<td><a href="baca-kontak/{{ $k->id }}"><i class="fa fa-envelope"></i></a>&nbsp;
					<a href="hapus_kontak/{{ $k->id }}"><i class="fa fa-trash"></i></a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

{{ $kontak->links()}}

@endsection
