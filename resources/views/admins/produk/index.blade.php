@extends('layouts.admin')

@section('title')
{{ $title }}
@endsection

@section('subtitle')
{{ $subtitle }}
@endsection

@section('content')


<h1><b>{{ $header }}</b></h1>
<hr>
@if(session()->has('message'))
<div class="alert alert-success">
	{{ session()->get('message') }}
</div>
@endif
<div class="tambah">
  <a href="{{ route('produk.create') }}" class="btn btn-primary">Tambah Baru</a>
</div>
<div class="table-responsive">
	<table class="table table-bordered" id="dataTable" width="100%">
		<thead>
			<th>ID</th>
			<th width="15%">Nama</th>
			<th width="12%">Gambar</th>
			<th>Kategori</th>
			<th>Keterangan</th>
			<th width="8%">Aksi</th>
		</thead>
		<tbody>

      @foreach($produk as $p)

			<tr>
				<td>{{ $p->id }}</td>
				<td>{{ $p->nama }}</td>
				<td><img style="width:100%" src="{{ URL::to('/') }}/images/produk/{{ $p->gambar }}"></td>
				<td>{{ $p->kategori }}</td>
				<td>{{ str_limit($p->keterangan, 300) }}</td>
				<td><a href="{{ route('produk.edit', $p->id) }}"><i class="fa fa-edit"></i></a>&nbsp;

  			  {!! Form::open(['method' => 'POST','route' => ['produk.destroy', $p->id],'style'=>'display:inline']) !!}
					{{ csrf_field() }}

					@method('DELETE')
						<button class="no-btn btn" type="submit"><i class="fa fa-trash"></i></button>
					{{ Form::close() }}
				</td>
			</tr>
      @endforeach

		</tbody>
	</table>
</div>

@endsection
