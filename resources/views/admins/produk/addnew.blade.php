@extends('layouts.admin')

@section('title')
{{ $title }}
@endsection

@section('subtitle')
{{ $subtitle }}
@endsection

@section('content')


<h1><b>{{ $header }}</b></h1>
<hr>
@if(session()->has('message'))
<div class="alert alert-success">
  {{ session()->get('message') }}
</div>
@endif

<div>
  {{ Form::open(array('id' => 'form-produk', 'route' => 'produk.store', 'method' => 'POST')) }}

  <div class="form-group row">
  {!! Form::label('nama', 'Nama', array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
  <div class="col-md-5">
   {!! Form::text('nama', null, array('class' => 'form-control', 'placeholder' => 'Nama Produk', 'required' => '')) !!}
 </div>
</div>

<div class="form-group row">
  {!! Form::label('gambar', 'Gambar',array('class' => 'data-gambar col-md-3 text-right d-none d-md-block control-label')) !!}
  <div class="col-md-5">
    {{ Form::select('gambar', $arr_filename, null,  array('id' => 'gambar-produk-src', 'class' => 'form-control', 'required' => '')) }}
      <img src="{{ URL::to('/') }}/images/produk/no-pict.jpg" id="gambar-produk" class="img-fluid" alt="Responsive image">
  </div>
  <div class="offset-md-3 col-md-5">
  </div>
</div>

<div class="form-group row">
{!! Form::label('kat_id', 'Kategori', array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
<div class="col-md-5">
 {{ Form::select('kat_id', $kategori, null, array('class'=>'form-control', 'required' => '')) }}
</div>
</div>

 <div class="form-group row">
  {!! Form::label('keterangan', 'Keterangan',array('class' => 'col-md-3 text-right d-none d-md-block control-label')) !!}
  <div class="col-md-5">
   {!! Form::textarea('keterangan', null, array('id' => 'keterangan', 'class' => 'form-control', 'placeholder' => 'Keterangan', 'required' => '')) !!}
 </div>
</div>

<div class="form-group row">
  <div class="offset-md-3 col-md-5">
    {!! Form::submit('Tambah', array('class' => 'btn btn-primary  control-label')) !!}
    {!! Form::reset('Batal', array('id' => 'reset', 'class' => 'btn btn-danger  control-label')) !!}
  </div>
</div>


{{ Form::close() }}
</div>



@endsection
