@extends('layouts.admin')

@section('title')
    {{ $title }}
@endsection

@section('subtitle')
    {{ $subtitle }}
@endsection

@section('content')


    <h1><b>{{ $header }}</b></h1>
    <hr>
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="tambah">
        <a href="{{ route('carousel.create') }}" class="btn btn-primary">Tambah Baru</a>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%">
            <thead>
            <th width="5%">ID</th>
            <th width="35%">Nama</th>
            <th>Gambar</th>
            <th width="8%">Aksi</th>
            </thead>
            <tbody>

            @foreach($carousel as $c)
                <tr>
                    <td>{{ $c->id }}</td>
                    <td>{{ $c->nama }}</td>
                    <td><img style="width:100%" src="{{ URL::to('/') }}/images/carousel/{{ $c->gambar }}"></td>
                    <td><a href="{{ route('carousel.edit', $c->id) }}"><i class="fa fa-edit"></i></a>&nbsp;

                        {!! Form::open(['method' => 'POST','route' => ['carousel.destroy', $c->id],'style'=>'display:inline']) !!}
                        {{ csrf_field() }}

                        @method('DELETE')
                        <button class="no-btn btn" type="submit"><i class="fa fa-trash"></i></button>
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>

@endsection
